/*
 * Copyright (C) 2018 GPIAY
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   main.cpp
 * Author: GPIAY
 *
 * Created on November 16, 2018, 2:20 PM
 */

#include <cstdlib>
#include <climits>
#include <fcntl.h>
#include <sys/stat.h>
#include <string>
#include <fstream>
#include <set>
#include <memory>

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>
#include <io.h>

#define PATH_MAX MAX_PATH
#else
#include <unistd.h>
#include <dirent.h>
#endif

#include <openslide/openslide.h>
#include <stdlib.h>
#include <sqlite3.h>
#include "cmdline.h"
#include "../../config.h"
#if (defined(__APPLE__) && defined(__MACH__)) || defined(WIN32)
#include <json/json.h>
#else
#include <jsoncpp/json/json.h>
#endif

#include "libmi.h"
#include "mmsi.h"

#ifdef WIN32
#define realpath(A, B) _fullpath(B, A, MAX_PATH)
#define mkdir(A, B) mkdir(A)

#define	R_OK 4
#define	W_OK 2
#define	X_OK 1
#define	F_OK 0
#endif

int mkdirs(const std::string &directoryPath) {
    int dirPathLen = directoryPath.length();
    char tmpDirPath[PATH_MAX] = { 0 };
    
    for (int i = 0; i < dirPathLen; ++i) {
        tmpDirPath[i] = directoryPath[i];
        
        if (tmpDirPath[i] == '/' || dirPathLen == i + 1) {
            if (access(tmpDirPath, F_OK) != 0) {
                int ret = mkdir(tmpDirPath, 0755);
                
                if (ret != 0) {
                    return ret;
                }
            }
        }
    }
    return 0;
}

int copy_file(const char *dst, const char *src) {
    std::ifstream srcf(src, std::ios::in | std::ios::binary | std::ios::ate);
    if (!srcf.good()) return 1;
    std::ofstream dstf(dst, std::ios::out | std::ios::binary);
    if (!dstf.good()) return 1;
    
    size_t size = srcf.tellg();
    srcf.seekg(0, std::ios::beg);
    
    char buf[4096];
    for (size_t i = 0; i < size; i += 4096) {
        size_t len = std::min((size_t) 4096, size - i);
        srcf.read(buf, len);
        dstf.write(buf, len);
    }
    
    srcf.close();
    dstf.close();
    return 0;
}

int write_json_file(const Json::Value &json, const std::string &file) {
    Json::StreamWriterBuilder jsrocd;
    std::unique_ptr<Json::StreamWriter> write(jsrocd.newStreamWriter());
    std::ostringstream os;
    write->write(json, &os);
    std::string strWrite = os.str();
    std::ofstream ofs;
    ofs.open(file);
    if (!ofs.good())
        return 1;
    ofs << strWrite;
    ofs.close();
    return 0;
}

int init_abort(const std::string &msg, std::string projdir = "") {
    std::cerr << msg << std::endl;
    
    if (projdir.length() > 0) {
        const char *pathname = projdir.data();
        
#ifdef WIN32
		WIN32_FIND_DATAA findFileData;
		HANDLE hFindFile = FindFirstFileA((projdir + "\\*").data(), &findFileData);
		if (hFindFile != INVALID_HANDLE_VALUE) {
			do {
				if (!(findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
					std::remove((projdir + "\\" + findFileData.cFileName).data());
				}
			} while (FindNextFileA(hFindFile, &findFileData));
		}
		FindClose(hFindFile);
#else
        DIR *dfd;
        char name[PATH_MAX];
        struct dirent *dp;
        if ((dfd = opendir(pathname)) == NULL)
            exit(0);
        while ((dp = readdir(dfd)) != NULL) {
            if (strncmp(dp->d_name, ".", 1) == 0)
                continue;
            snprintf(name, PATH_MAX, "%s/%s", pathname, dp->d_name);
            std::remove(name);
        }
        closedir(dfd);
#endif
        rmdir(pathname);
    }
    
    exit(0);
}

int main(int argc, char** argv) {
    cmdline::parser p;
    
    p.add<std::string>("input", 'i', "input whole-slide image file path", true, "");
    p.add<std::string>("output", 'o', "output project directory path", true, "");
    
    p.add<int>("segm", 's', "number of bytes per id for segmentation", false, 1, cmdline::oneof(1, 2, 4, 8));
    p.add<int>("tile", 't', "tile size of segm data", false, 256, cmdline::range(64, 65536));
    p.add<int>("grade", 'g', "number of bytes per grade chunk", false, 0x100000, cmdline::range(0x400, 0x40000000));
    p.add("copy", 'c', "copy image file to project folder");
    
    p.parse_check(argc, argv);
    
    std::string inputfile = p.get<std::string>("input");
    std::string projdir = p.get<std::string>("output");
    
    if (access(inputfile.data(), R_OK | F_OK))
        init_abort("Unable to find file " + inputfile + ".");
    
    if (!access(projdir.data(), F_OK))
        init_abort("Directory " + projdir + " already exists.");
    
    Json::Value proj_json;
    bool copyfile = p.exist("copy");
    int segm_pxw = p.get<int>("segm");
    int tile_size = p.get<int>("tile");
    int grade_size = p.get<int>("grade");
    proj_json["segm_pxw"] = segm_pxw;
    proj_json["tile_size"] = tile_size;
    proj_json["grade_size"] = grade_size;
    
    char temppath[PATH_MAX];
    auto ret = realpath(projdir.data(), temppath);
    projdir = temppath;
    
    if (mkdirs(projdir))
        init_abort("Create project directory failed.");
    
    Json::Value obj_wsi;
    obj_wsi["copy"] = copyfile;
    if (copyfile) {
        std::string inputfile_name;
        int cdir = inputfile.rfind("/");
        if (cdir >= 0)
            inputfile_name = inputfile.substr(cdir);
        else inputfile_name = inputfile;
        
        std::string inputfile_copy = projdir + "/" + inputfile_name;
        if (copy_file(inputfile_copy.data(), inputfile.data()))
            init_abort("Failed to copy image file.", projdir);
        obj_wsi["file"] = inputfile_name;
        inputfile = inputfile_copy;
    } else {
        ret = realpath(inputfile.data(), temppath);
        inputfile = temppath;
        obj_wsi["file"] = inputfile;
    }
    
    obj_wsi["type"] = "unknown";
    openslide_t *openslide = openslide_open(inputfile.data());
    if (openslide != nullptr) {
        if (openslide_get_error(openslide) == nullptr) {
            obj_wsi["type"] = "openslide";
        } else {
            openslide_close(openslide);
            openslide = nullptr;
        }
    }
    
    Json::Value obj_level;
    if (obj_wsi["type"] == "openslide") {
        int n = openslide_get_level_count(openslide);
        for (int i = 0; i < n; i++) {
            int64_t w, h;
            openslide_get_level_dimensions(openslide, i, &w, &h);
            Json::Value size;
            size["width"] = static_cast<Json::Int64>(w);
            size["height"] = static_cast<Json::Int64>(h);
            obj_level.append(size);
        }
        
        Json::Value property;
        auto props = openslide_get_property_names(openslide);
        for (int i = 0; props[i]; i++) {
            property[props[i]] = openslide_get_property_value(openslide, props[i]);
        }
        obj_wsi["property"] = property;
    } else
    if (obj_wsi["type"] == "dicom") {
        
    } else
    if (obj_wsi["type"] == "unknown")
        init_abort("Unknown whole-slide image format.", projdir);
    
    proj_json["level"] = obj_level;
    proj_json["wsi"] = obj_wsi;
    
    Json::Value arr_anno;
    
    proj_json["anno"] = arr_anno;
    proj_json["version"] = FILE_VERSION;
    
    mmsi::Writer segmw(projdir + "/segm.mmsi");
    segmw.SetImageSize(obj_level[0]["width"].asInt64(), obj_level[0]["height"].asInt64());
    segmw.SetTileSize(tile_size, tile_size);
    segmw.SetPixelSize(segm_pxw);
    if (!segmw.OpenFile())
        init_abort("Failed to create segmentation MMSI file.", projdir);
    
    if (!segmw.CloseFile())
        init_abort("Failed to save segmentation MMSI file.", projdir);
    
    sqlite3 *gradedb;
    if (SQLITE_OK != sqlite3_open_v2((projdir + "/grade.db").data(), &gradedb, SQLITE_OPEN_READWRITE |
        SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_SHAREDCACHE, nullptr))
        init_abort("Failed to create grading database.", projdir);
    if (SQLITE_OK != sqlite3_exec(gradedb, "CREATE TABLE openhil_grade (tile_id INTEGER PRIMARY KEY, data BLOB);", nullptr, nullptr, nullptr))
        init_abort("Failed to create database table.", projdir);
    if (SQLITE_OK != sqlite3_close(gradedb))
        init_abort("Failed to save database file.", projdir);
    
    if (write_json_file(proj_json, projdir + "/project.json"))
        init_abort("Failed to write project json file.", projdir);
    
    if (openslide != nullptr)
        openslide_close(openslide);

    return 0;
}
