import loci.formats.FormatException;
import loci.formats.gui.BufferedImageReader;
import loci.formats.tiff.TiffParser;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            BufferedImageReader reader = new BufferedImageReader();
            reader.setId("TCGA-GK-A6C7-01Z-00-DX1.291DEE77-CD13-4D8E-BA35-6EC15F45D264.svs");
            long amount = 0;
            Scanner in = new Scanner(new FileInputStream(new File("input20.txt")));
            while (in.hasNextLine()) {
                String[] arg = in.nextLine().split(" ");
                int layer = Integer.parseInt(arg[1]);
                int dsr = 1 << layer;
                reader.setSeries(layer);
                long time = System.currentTimeMillis();
                BufferedImage image = reader.openImage(0, Integer.parseInt(arg[2]) / dsr, Integer.parseInt(arg[3]) / dsr, Integer.parseInt(arg[4]), Integer.parseInt(arg[5]));
                time = System.currentTimeMillis() - time;
                System.out.printf("%s %d\n", arg[0], time);
                amount += time;
            }
            System.out.println(amount);
        } catch (IOException | FormatException e) {
            e.printStackTrace();
        }
    }
}
