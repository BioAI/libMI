#!/usr/bin/python3

import mmsi
import openslide
import numpy
import cv2

# Open an openslide image as the source data.
oslide = openslide.OpenSlide('TCGA-B0-5088-11A-01-TS1_481546e6-ba46-4c6a-9998-d7d0e0c84b74.svs')

# Create a mmsi file using mmsi Writer class.
mslide = mmsi.Writer('result.mmsi')

# Set the image dimensions.
mslide.SetImageSize(oslide.dimensions)

# Set the mmsi file tile size.
mslide.SetTileSize((256, 256))

# Set the image bit count (4 for RGBA image).
mslide.SetPixelSize(4)

# Set number of levels in the pyramid image.
mslide.SetLevelCount(4)

# Open and Close to create the file.
mslide.Open()
mslide.Close()

# Open the file using mmsi Accessor to operate the file.
mslide = mmsi.Accessor('result.mmsi')

# For an accessor class, we must open it first to do other actions.
mslide.Open()

# Get the image dimensions.
msize = (mslide.GetImageWidth(), mslide.GetImageHeight())

# Tile by tile read the data from source image and write to the mmsi file.
# The operating region size is 1024 x 1024.
x = 0
while x < oslide.dimensions[0]:
    y = 0
    while y < oslide.dimensions[1]:

        # Calculate the region size.
        w = min(1024, oslide.dimensions[0] - x)
        h = min(1024, oslide.dimensions[1] - y)

        # Read data from source image and do thresholding.
        tile = numpy.array(oslide.read_region((x, y), 0, (w, h)))

        # Write data to mmsi file level 0.
        mslide.SetCurrentRegion((x, y), (w, h))
        mslide.WriteRegion(0, tile, None)

        print('converting %d, %d' % (x, y))

        y = y + 1024
    x = x + 1024

# Reading completed. Now we can close the source file.
oslide.close()

# Get the overview of the generated mmsi image.
mslide.SetCurrentRegion((0, 0), msize)
print('writing result image')

# Write the overview image.
data = mslide.ReadRegion(3)
data.dtype = numpy.uint8
data = data.reshape((msize[1] // 8, msize[0] // 8, 4))
data = cv2.cvtColor(data, cv2.COLOR_BGRA2RGBA)
cv2.imwrite('overview.png', data)

# Close the file to save the image data.
print('writing image data')
mslide.Close()
