#!/usr/bin/python3

import mmsi
import openslide
import numpy
import cv2

# Open an openslide image as the source data.
oslide = openslide.OpenSlide('TCGA-B0-4839-01A-01-BS1_9ef02e09-8670-4d9c-8ea3-b40060e56e41.svs')

# Create a mmsi file using mmsi Writer class.
mslide = mmsi.Writer('threshold.mmsi')

# Set the image dimensions.
mslide.SetImageSize(oslide.dimensions)

# Set the mmsi file tile size.
mslide.SetTileSize((256, 256))

# Set the image bit count (1 for 8-bit image).
mslide.SetPixelSize(1)

# Set number of levels in the pyramid image.
mslide.SetLevelCount(4)

# Open and Close to create the file.
mslide.Open()
mslide.Close()

# Open the file using mmsi Accessor to operate the file.
mslide = mmsi.Accessor('threshold.mmsi')

# For an accessor class, we must open it first to do other actions.
mslide.Open()

# Get the image dimensions.
msize = (mslide.GetImageWidth(), mslide.GetImageHeight())

# Tile by tile read the data from source image and write to the mmsi file.
# The operating region size is 1024 x 1024.
x = 0
while x < oslide.dimensions[0]:
    y = 0
    while y < oslide.dimensions[1]:

        # Calculate the region size.
        w = min(1024, oslide.dimensions[0] - x)
        h = min(1024, oslide.dimensions[1] - y)

        # Read data from source image and do thresholding.
        tile = numpy.array(oslide.read_region((x, y), 0, (w, h)))
        ret, tile = cv2.threshold(tile, 128, 255, cv2.THRESH_BINARY)
        tile = tile[...,1]

        # Write data to mmsi file level 0.
        mslide.SetCurrentRegion((x, y), (w, h))
        mslide.WriteRegion(0, tile, None)

        print('thresholding %d, %d' % (x, y))

        y = y + 1024
    x = x + 1024

# Reading completed. Now we can close the source file.
oslide.close()

# Get the overview of the generated mmsi image.
mslide.SetCurrentRegion((0, 0), msize)
print('writing result image')

# Read level 3 to get the top-level image.
data = mslide.ReadRegion(3)
cv2.imwrite('threshold.png', data)

# Close the file to save the image data.
print('writing image data')
mslide.Close()

