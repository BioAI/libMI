#!/usr/bin/python3

import libmi
import cv2
import numpy

# Load the created LibMI project.
slide = libmi.Slide('proj_freehand')
slide.LoadProject()

# Read two freehand annotations from image files.
fig1 = cv2.imread('freehand1.png')
fig2 = cv2.imread('freehand2.png')

# Get and insert the alpha channel since we need 32-bit images.
alpha1 = cv2.bitwise_not(fig1[...,0])
alpha2 = cv2.bitwise_not(fig2[...,1])
fig1 = numpy.insert(fig1, 3, values = alpha1, axis = 2)
fig2 = numpy.insert(fig2, 3, values = alpha2, axis = 2)

# Write the first annotation from (0, 0) to (2048, 2048).
slide.SetCurrentRegion((0, 0), (2048, 2048))
slide.SetRegionSize((512, 512))
slide.WriteSegm(fig1, alpha1)

# Write the second annotation from (1024, 1024) to (3072, 3072).
slide.SetCurrentRegion((1024, 1024), (2048, 2048))
slide.SetRegionSize((1024, 1024))
slide.WriteSegm(fig2, alpha2)

# Read the final annotation as a 512 x 512 image.
slide.SetCurrentRegion((0, 0), (3072, 3072))
slide.SetRegionSize((512, 512))
fig3 = slide.ReadSegm()

# Reshape the array from int64 array to RGBA.
fig3.dtype = numpy.uint8
fig3 = fig3.reshape((512, 512, 4))

# Write the image file and close the project to save data.
cv2.imwrite('freehand.png', fig3)
slide.SaveProject()

