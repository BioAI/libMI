#!/usr/bin/python3

"""
Implementation of [1] A Jain. Fundamentals of digital image processing. Prentice-Hall, 1989.

Created by Pargorn Puttapirat (pargorn.puttapirat@g.swu.ac.th)& Jingyi Deng at XJTU
Created on 20190102
"""
import cv2 as cv    # Import OpenCV
import numpy as np
import libmi
import openslide
import matplotlib.pyplot as plt

# Open the source and target LibMI project to do color correction.
source = libmi.Slide('source')
target = libmi.Slide('target')

source.LoadProject()
target.LoadProject()


def hist_match(source, template):
    """
    Adjust the pixel values of a grayscale image such that its histogram
    matches that of a target image.
    Code adapted from
    http://stackoverflow.com/questions/32655686/histogram-matching-of-two-images-in-python-2-x
    Arguments:
    -----------
        source: np.ndarray
            Image to transform; the histogram is computed over the flattened
            array
        template: np.ndarray
            Template image; can have different dimensions to source
    Returns:
    -----------
        matched: np.ndarray
            The transformed output image

    Source: https://gist.github.com/jcjohnson/e01e4fcf7b7dfa9e0dbee6c53d3120b6
    """
    oldshape = source.shape
    
    ret_t,th_t = cv.threshold(template,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    template = template.ravel()
    template_obj=template[np.where(template<ret_t)]

    ret_s,th_s = cv.threshold(source,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    source = source.ravel()
    source_ind = np.where(source < ret_s)
    source_obj=source[np.where(source<ret_s)]

    # get the set of unique pixel values and their corresponding indices and
    # counts
    s_obj_values, bin_idx, s_counts = np.unique(source_obj,return_inverse=True,
                                            return_counts=True)

    t_values, t_counts = np.unique(template_obj, return_counts=True)

    # take the cumsum of the counts and normalize by the number of pixels to
    # get the empirical cumulative distribution functions for the source and
    # template images (maps pixel value --> quantile)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    j = 0
    for i in np.nditer(source_ind):
        source[i]=interp_t_values[bin_idx][j]
        j=j+1

    return source.reshape(oldshape)


# Read the original and target images
# TODO: -- Do remember to change the image before publish !!!!
x = 0
while x < source.GetWidth():
    y = 0
    while y < source.GetHeight():

        w = min(1000, source.GetWidth() - x)
        h = min(1000, source.GetHeight() - y)

        source.SetCurrentRegion((x, y), (w, h))
        source.SetRegionSize((w, h))
        tile = source.ReadRegion()

        target.SetCurrentRegion((0, 0), (target.GetWidth(), target.GetHeight()))
        target.SetRegionSize((1000, 1000))
        tar_top = target.ReadRegion()

        try:
            # Output image
            img_result = tile
            # Runt the loop through BGR channels
            for k in range(3):
                img_result[:, :, k] = hist_match(tile[:, :, k], tar_top[:, :, k])

            source.WriteSegm(img_result,None)

            print('color correction: %d, %d' % (x, y))
        except IndexError:
            print('backgroud: %d, %d' % (x, y))
        
        y = y + 1000
    x = x + 1000

# Read the result image thumbnail and output the result.
source.SetCurrentRegion((0, 0), (source.GetWidth(), source.GetHeight()))
resolution = (int(source.GetWidth() / 100), int(source.GetHeight() / 100))
source.SetRegionSize(resolution)

source_cor = source.ReadSegm()
source_cor.dtype = np.uint8
source_cor = source_cor.reshape((resolution[1], resolution[0], 4))
cv.imwrite('correction_res.png',source_cor)

source.SaveProject()
target.SaveProject()

