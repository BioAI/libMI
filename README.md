# libMI

libMI is a C++ library for annotating histopathological images.

Documentation can be found at [here](https://bioai.gitlab.io/libMI-docs/).

## Requirements

- JsonCpp >= 1.7.4
- OpenCV >= 3.2.0
- OpenSlide >= 3.4.1
- Sqlite3 >= 3.22.0
- zlib >= 1.2.11
- automake, autoconf, libtool and pkg-config if building from Git repository.
- Python >= 3.6
- Numpy

## How to build

### Linux & macOS

```
autoreconf -i
./configure
make
make install
```

### Windows

* Download pre-compiled binaries from [here](https://bioai.gitlab.io/libMI-docs/).
* Add the bin directory to PATH.

### Python bindings

```
pip install libmi
pip install mmsi
```

## FAQ
* Getting error during `make install`
    * Error message (last line of output): `make: *** [install-recursive] Error 1` 
    * Solution: try to run the command again with root access: `sudo make install`
* Cannot import _libMI_ in python using `import libmi`
    * Error message: `OSError: libmi.so.0: cannot open shared object file: No such file or directory`
    * Solution: Add `/usr/local/lib` to `/etc/ld.so.conf` and run `ldconfig` before executing the import command.
        1. Open the file with root permission" `nano /etc/ld.so.conf`
        2. Add the path at the end of the file: `include /usr/local/lib`
        3. Write the file. 
        4. Run: `ldconfig`
        