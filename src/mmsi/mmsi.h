/*
 * Copyright (C) 2018 GPIAY
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   mmsi.h
 * Author: GPIAY
 *
 * Created on December 3, 2018, 2:16 PM
 */

#ifndef MMSI_H
#define MMSI_H

#if defined(_MSC_VER) || defined(_WIN32) || defined(_WIN64)
#ifdef MMSI_EXPORTS
#define MMSI_EXPORT __declspec(dllexport)
#else
#define MMSI_EXPORT __declspec(dllimport)
#endif
#else
#define MMSI_EXPORT
#endif

#ifdef __cplusplus
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#else
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#ifdef __cplusplus

#define MMSIPROP_VERSION        "mmsi_version"
#define MMSIPROP_IMAGE_WIDTH    "mmsi_width"
#define MMSIPROP_IMAGE_HEIGHT   "mmsi_height"
#define MMSIPROP_TILE_WIDTH     "mmsi_tile_width"
#define MMSIPROP_TILE_HEIGHT    "mmsi_tile_height"
#define MMSIPROP_LEVEL_COUNT    "mmsi_level_count"
#define MMSIPROP_PIXEL_SIZE     "mmsi_pixel_size"
#define MMSIPROP_DESCRIPTION    "mmsi_description"

#define MMSIMODE_PNG    0
#define MMSIMODE_JPG    1
#define MMSIMODE_ZLIB   2

namespace mmsi {
    
/**
 * Only for internal usage.
 */
class Layer;

/**
 * Only for internal usage.
 */
class Tile;

/**
 * Only for internal usage.
 */
class FilePrivate;

/**
 * The base class of mmsi operating classes.
 */
class MMSI_EXPORT File {
    friend class Layer;
    friend class Tile;
public:
    
    /**
     * The constructor of File class. Only for internal usage.
     * 
     * @param filename The image file name.
     */
    File(const std::string &filename);
    virtual ~File();
    
    /**
     * Load file data or create file. Must be called before reading or writing
     * actions.
     * 
     * @return Succeeded or not.
     */
    virtual bool OpenFile();
    
    /**
     * Save file data to disk. After calling this function the only useful action
     * is to release this object.
     * 
     * @return Succeeded or not.
     */
    virtual bool CloseFile();

    /**
     * Get image property value.
     * 
     * @param name The property name.
     * @return The property value.
     */
    const std::string GetPropertyValue(const std::string &name);
    
    /**
     * Set image property value.
     * 
     * @param name The property name.
     * @param value The property value.
     */
    void SetPropertyValue(const std::string &name, const std::string &value);
    
    /**
     * Remove the property from this image.
     * 
     * @param name The property name.
     */
    void RemoveProperty(const std::string &name);
    
    /**
     * Is the property existed in this image.
     * 
     * @param name The property name.
     * @return Existed or not.
     */
    bool IsPropertyExisted(const std::string &name);
    
    /**
     * Get all property names in this image.
     * 
     * @return The property names.
     */
    const std::vector<std::string> GetPropertyNames();

    /**
     * Get the description of this image.
     * 
     * @return The description.
     */
    std::string GetDescription();
    
    /**
     * Set the description of this image.
     * 
     * @param desc The description.
     */
    void SetDescription(const std::string &desc);
    
    /**
     * Has any error occurred.
     * 
     * @return Any error occurred or not.
     */
    bool HasError();
    
    /**
     * Get the error string.
     * 
     * @return The error string.
     */
    const std::string &GetError();
    
    /**
     * Set the data compression level (1 - 9). Set 1 for the minimal compression
     * time and set 9 for the minimal file size.
     * 
     * @param level The compression level;
     */
    void SetCompressLevel(int32_t level);

    /**
     * Set number of threads for parallel processing. Default thread number is 1.
     *
     * @param num Thread Number.
     */
    void SetThreadNum(int32_t num);

    /**
     * Set compression mode between JPG, PNG and ZLIB. Default mode is PNG.
     *
     * @param mode Compression Mode. One of MMSIMODE_PNG, MMSIMODE_JPG and MMSIMODE_ZLIB.
     */
    void SetCompressMode(int32_t mode);

protected:
    
    FilePrivate *m;
};

/**
 * The class used to generate new mmsi files.
 */
class MMSI_EXPORT Writer : public File {
    friend class Layer;
    friend class Tile;
public:
    
    /**
     * The constructor of Writer class.
     * 
     * @param filename The image file name.
     */
    Writer(const std::string &filename);
    virtual ~Writer();

    /**
     * Set the resolution of this image.
     * 
     * @param width The image width.
     * @param height The image height.
     */
    void SetImageSize(int64_t width, int64_t height);
    
    /**
     * Set the image tile size.
     * 
     * @param width The tile width.
     * @param height The tile height.
     */
    void SetTileSize(int64_t width, int64_t height);
    
    /**
     * Set how many bytes there are in one pixel.
     * 
     * @param size Size in bytes. Must be one of (1, 2, 4, 8).
     */
    void SetPixelSize(int32_t size);
    
    /**
     * Set how many layers this image contains.
     * 
     * @param count The number of layers.
     */
    void SetLevelCount(int32_t count);

    /**
     * Initialize image data in certain region.
     * 
     * This function must be called after OpenFile is called and before CloseFile
     * is called.
     * 
     * @param data The data buffer. Must be a valid pointer with at least (w * h
     * * pixel size) bytes of space.
     * @param mask The mask buffer. Must be at least (w * h) bytes of space. Can
     * be null to ignore this parameter.
     * @param x The x coordination of the top-left corner.
     * @param y The y coordination of the top-left corner.
     * @param w The width of the region.
     * @param h The height of the region.
     */
    void InitRegion(void *data, void *mask, int64_t x, int64_t y, int64_t w, int64_t h);

    /**
     * Create the file and wait to give initial image data.
     * 
     * This function must be called after image parameters such as resolution and
     * tile size have been assigned.
     * 
     * @return Succeeded or not.
     */
    virtual bool OpenFile() override;
    
    /**
     * Finish initializing image data and write to disk.
     * 
     * @return Succeeded or not.
     */
    virtual bool CloseFile() override;

private:
    Layer *InitLayer(int32_t level);

    int64_t m_tile_cnt = 0;
};

/**
 * The class used to read and write mmsi files.
 */
class MMSI_EXPORT Accessor : public File {
    friend class Layer;
    friend class Tile;
public:
    
    /**
     * The constructor of Accessor class.
     * 
     * @param filename The image file name.
     */
    Accessor(const std::string &filename);
    virtual ~Accessor();

    /**
     * Load image information. Must be called first.
     * 
     * @return Succeeded or not.
     */
    virtual bool OpenFile() override;
    
    /**
     * Close the file and update file data.
     * 
     * @return Succeeded or not.
     */
    virtual bool CloseFile() override;

    /**
     * Set the current operating region.
     * 
     * @param x The x coordination of the top-left corner.
     * @param y The y coordination of the top-left corner.
     * @param w The width of the region.
     * @param h The height of the region.
     */
    void SetCurrentRegion(int64_t x, int64_t y, int64_t w, int64_t h);
    
    /**
     * Get image width for certain layer.
     * 
     * @param level The level of layer.
     * @return The image width.
     */
    int64_t GetRegionWidthLevel(int32_t level);
    
    /**
     * Get image height for certain layer.
     * 
     * @param level The level of layer.
     * @return The image height.
     */
    int64_t GetRegionHeightLevel(int32_t level);
    
    /**
     * Get down sample ratio for certain layer.
     * 
     * @param level The level of layer.
     * @return The down sample ratio.
     */
    int64_t GetDownsample(int32_t level);

    /**
     * Read image data from file.
     * 
     * @param level The level of layer to read.
     * @param buf The data buffer, must be a valid pointer with at least [(w /
     * down sample) * (h / down sample) * pixel size] bytes of space.
     */
    void ReadRegion(int32_t level, void *buf);
    
    /**
     * Write data to the file.
     * 
     * @param level The level of layer to write.
     * @param buf The data buffer, must be a valid pointer with at least [(w /
     * down sample) * (h / down sample) * pixel size] bytes of space.
     * @param mask The mask buffer, must be at least [(w / down sample) * (h /
     * down sample)] bytes of space. Can be null to ignore this parameter.
     */
    void WriteRegion(int32_t level, void *buf, void *mask);

    /**
     * Get the image width.
     * 
     * @return The image width.
     */
    int64_t GetImageWidth();
    
    /**
     * Get the image height.
     * 
     * @return The image height.
     */
    int64_t GetImageHeight();
    
    /**
     * Get image tile width.
     * 
     * @return The tile width.
     */
    int64_t GetTileWidth();
    
    /**
     * Get image tile height.
     * 
     * @return The tile height.
     */
    int64_t GetTileHeight();
    
    /**
     * Get image pixel size in bytes.
     * 
     * @return The pixel size.
     */
    int32_t GetPixelSize();
    
    /**
     * Get number of layers in this image.
     * 
     * @return The number of layers.
     */
    int32_t GetLevelCount();

    /**
     * Get current cache size.
     * 
     * @return The cache size.
     */
    int32_t GetCacheSize();
    
    /**
     * Set the cache size, which means how many tiles should be kept in memory
     * to increase performance.
     * 
     * @param size The cache size.
     */
    void SetCacheSize(int32_t size);
    
    /**
     * Turn on / off secure mode. Secure mode means the file data will not be
     * corrupted even if the program crushes. Caching will be disabled when secure
     * mode is activated.
     * 
     * @param enable Enable secure mode or not.
     */
    void SetSecureMode(bool enable);

private:
    void Downward(int32_t low);
    void Upward(int32_t high);

    void ClearCache(bool force);

    struct {
        int64_t x = 0, y = 0, w = 0, h = 0;
    } m_roi;
    int32_t m_cache_size;
    bool m_secure_mode;

    std::set<std::pair<int32_t, Tile*>> m_mod_tiles;
};
    
extern "C" {
#endif
    
typedef struct MMSI_t MMSI;

/**
 * Get the mmsi library version string.
 * 
 * @return Version.
 */
MMSI_EXPORT const char *mmsi_Version();

MMSI_EXPORT MMSI *mmsi_Writer(const char *filename);
MMSI_EXPORT MMSI *mmsi_Accessor(const char *filename);
MMSI_EXPORT void mmsi_Destroy(MMSI *mmsi);

MMSI_EXPORT bool mmsi_Open(MMSI *mmsi);
MMSI_EXPORT bool mmsi_Close(MMSI *mmsi);

MMSI_EXPORT const char *mmsi_GetPropertyValue(MMSI *mmsi, const char *key);
MMSI_EXPORT void mmsi_SetPropertyValue(MMSI *mmsi, const char *key, const char *value);
MMSI_EXPORT void mmsi_RemoveProperty(MMSI *mmsi, const char *key);
MMSI_EXPORT bool mmsi_IsPropertyExisted(MMSI *mmsi, const char *key);
MMSI_EXPORT const char *const *mmsi_GetPropertyNames(MMSI *mmsi);

MMSI_EXPORT const char *mmsi_GetDescription(MMSI *mmsi);
MMSI_EXPORT void mmsi_SetDescription(MMSI *mmsi, const char *desc);
    
MMSI_EXPORT bool mmsi_HasError(MMSI *mmsi);
MMSI_EXPORT const char *mmsi_GetError(MMSI *mmsi);

MMSI_EXPORT void mmsi_SetCompressLevel(MMSI *mmsi, int32_t level);
MMSI_EXPORT void mmsi_SetThreadNum(MMSI* mmsi, int32_t num);
MMSI_EXPORT void mmsi_SetCompressMode(MMSI* mmsi, int32_t mode);

MMSI_EXPORT void mmsi_SetImageSize(MMSI *mmsi, int64_t width, int64_t height);
MMSI_EXPORT void mmsi_SetTileSize(MMSI *mmsi, int64_t width, int64_t height);
MMSI_EXPORT void mmsi_SetPixelSize(MMSI *mmsi, int32_t size);
MMSI_EXPORT void mmsi_SetLevelCount(MMSI *mmsi, int32_t count);

MMSI_EXPORT void mmsi_InitRegion(MMSI *mmsi, void *data, void *mask, int64_t x, int64_t y, int64_t w, int64_t h);
        
MMSI_EXPORT void mmsi_SetCurrentRegion(MMSI *mmsi, int64_t x, int64_t y, int64_t w, int64_t h);

MMSI_EXPORT int64_t mmsi_GetRegionWidthLevel(MMSI *mmsi, int32_t level);
MMSI_EXPORT int64_t mmsi_GetRegionHeightLevel(MMSI *mmsi, int32_t level);
MMSI_EXPORT int64_t mmsi_GetDownsample(MMSI *mmsi, int32_t level);

MMSI_EXPORT void mmsi_ReadRegion(MMSI *mmsi, int32_t level, void *buf);
MMSI_EXPORT void mmsi_WriteRegion(MMSI *mmsi, int32_t level, void *buf, void *mask);

MMSI_EXPORT int64_t mmsi_GetImageWidth(MMSI *mmsi);
MMSI_EXPORT int64_t mmsi_GetImageHeight(MMSI *mmsi);
MMSI_EXPORT int64_t mmsi_GetTileWidth(MMSI *mmsi);
MMSI_EXPORT int64_t mmsi_GetTileHeight(MMSI *mmsi);
MMSI_EXPORT int32_t mmsi_GetPixelSize(MMSI *mmsi);
MMSI_EXPORT int32_t mmsi_GetLevelCount(MMSI *mmsi);

MMSI_EXPORT int32_t mmsi_GetCacheSize(MMSI *mmsi);
MMSI_EXPORT void mmsi_SetCacheSize(MMSI *mmsi, int32_t size);
MMSI_EXPORT void mmsi_SetSecureMode(MMSI *mmsi, bool enable);

#ifdef __cplusplus
}
}
#endif

#endif /* MMSI_H */

