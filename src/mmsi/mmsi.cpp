/*
 * Copyright (C) 2018 GPIAY
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   mmsi.cpp
 * Author: GPIAY
 *
 * Created on December 3, 2018, 2:18 PM
 */

#include "ThreadPool.h"
#include <sqlite3.h>
#include <opencv2/opencv.hpp>
#include <zlib.h>

#include "mmsi.h"
#include "../../config.h"

#undef AddJob

#define MMSI_VERSION        VERSION
#define MMSI_FILE_VERSION   FILE_VERSION

using nbsdx::concurrent::ThreadPool;

std::mutex g_mutex;

int ERRCHECK_fn(int res, int line) {
    if (res != SQLITE_OK && res != SQLITE_ROW && res != SQLITE_DONE)
        throw std::string("Sqlite error: (") + sqlite3_errstr(res) + ") at mmsi.cpp line " + std::to_string(line) + ".";
    return res;
}
#define ERRCHK(_result) ERRCHECK_fn(_result, __LINE__)

std::pair<void*, int> ImgCompressPng(cv::Mat *mat, int pixelsize, int complevel) {
    std::vector<uchar> buf;
    std::vector<int> params{ CV_IMWRITE_PNG_COMPRESSION, complevel };
    cv::imencode(".png", *mat, buf, params);
    void* buffer = malloc(buf.size());
    memcpy(buffer, &buf[0], buf.size());
    return { buffer, buf.size() };
}

std::pair<void*, int> ImgCompressJpg(cv::Mat *mat, int pixelsize, int complevel) {
    if (pixelsize == 4) {
        std::vector<uchar> buf, alphabuf;
        std::vector<int> params{ CV_IMWRITE_JPEG_QUALITY, 100 - complevel * 5 };

        cv::imencode(".jpg", *mat, buf, params);
        int bufsize = buf.size();

        cv::Mat alpha;
        cv::extractChannel(*mat, alpha, 3);
        cv::imencode(".png", alpha, alphabuf, std::vector<int>{ CV_IMWRITE_PNG_COMPRESSION, complevel });

        uchar* buffer = static_cast<uchar*>(malloc(bufsize + alphabuf.size() + 4));
        memcpy(buffer, &bufsize, 4);
        memcpy(buffer + 4, &buf[0], bufsize);
        memcpy(buffer + 4 + bufsize, &alphabuf[0], alphabuf.size());
        return { buffer, bufsize + alphabuf.size() + 4 };
    }
    else {
        return ImgCompressPng(mat, pixelsize, complevel);
    }
}

std::pair<void*, int> ImgCompressZlib(cv::Mat *mat, int pixelsize, int complevel) {
    uint64_t odatalen = mat->total() * pixelsize;
    uint64_t datalen = compressBound(odatalen);
    uint8_t* databuf = static_cast<uint8_t*>(std::malloc(datalen));
    compress2(databuf, reinterpret_cast<unsigned long*>(&datalen), mat->data, odatalen, complevel);
    return { databuf, datalen };
}

typedef std::pair<void*, int>(*ImgCompressFunc)(cv::Mat*, int, int);

void ImgUncompressPng(cv::Mat* mat, const void *buffer, int len, int olen, int pixelsize) {
    std::vector<uchar> buf(static_cast<const uchar*>(buffer), static_cast<const uchar*>(buffer) + len);
    cv::imdecode(buf, CV_LOAD_IMAGE_UNCHANGED, mat);
}

void ImgUncompressJpg(cv::Mat* mat, const void* buffer, int len, int olen, int pixelsize) {
    if (pixelsize == 4) {
        int bufsize = *static_cast<const int*>(buffer);
        std::vector<uchar> buf(static_cast<const uchar*>(buffer) + 4, static_cast<const uchar*>(buffer) + bufsize + 4);
        std::vector<uchar> alphabuf(static_cast<const uchar*>(buffer) + bufsize + 4, static_cast<const uchar*>(buffer) + len);
        cv::Mat color = cv::imdecode(buf, CV_LOAD_IMAGE_UNCHANGED);
        cv::Mat alpha = cv::imdecode(alphabuf, CV_LOAD_IMAGE_UNCHANGED);
        cv::merge(std::vector<cv::Mat>{ color, alpha }, *mat);
    }
    else {
        std::vector<uchar> buf(static_cast<const uchar*>(buffer), static_cast<const uchar*>(buffer) + len);
        cv::imdecode(buf, CV_LOAD_IMAGE_UNCHANGED, mat);
    }
}

void ImgUncompressZlib(cv::Mat* mat, const void* buffer, int len, int olen, int pixelsize) {
    uncompress(mat->data, reinterpret_cast<unsigned long*>(&olen), static_cast<const uint8_t*>(buffer), len);
}

typedef void(*ImgUncompressFunc)(cv::Mat*, const void*, int, int, int);

const char sql_create_prop_table[] =
    "CREATE TABLE mmsi_props ("
        "prop_id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "prop_key VARCHAR(255) NOT NULL, "
        "prop_value LONGTEXT NOT NULL DEFAULT ''"
    ");";
const char sql_create_layer_table[] =
    "CREATE TABLE mmsi_layers ("
        "layer_id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "layer_width BIGINT NOT NULL, "
        "layer_height BIGINT NOT NULL, "
        "tile_start_id BIGINT NOT NULL, "
        "tile_x_count BIGINT NOT NULL, "
        "tile_y_count BIGINT NOT NULL"
    ");";
const char sql_create_tile_table[] =
    "CREATE TABLE mmsi_tiles ("
        "tile_id INTEGER PRIMARY KEY AUTOINCREMENT, "
        "lazy_flag BOOL NOT NULL, "
        "data BLOB, "
        "mask BLOB"
    ");";

const char sql_read_prop[] = "SELECT prop_value FROM mmsi_props WHERE prop_key = ?1;";
const char sql_write_prop[] = "UPDATE mmsi_props SET prop_value = ?2 WHERE prop_key = ?1;";
const char sql_insert_prop[] = "INSERT INTO mmsi_props (prop_key, prop_value) VALUES (?1, ?2);";
const char sql_delete_prop[] = "DELETE FROM mmsi_props WHERE prop_key = ?1;";
const char sql_count_prop[] = "SELECT prop_key FROM mmsi_props;";

const char sql_insert_layer[] = "INSERT INTO mmsi_layers (layer_width, layer_height, tile_start_id, tile_x_count, tile_y_count) VALUES (?, ?, ?, ?, ?);";
const char sql_query_layer[] = "SELECT layer_width, layer_height, tile_start_id, tile_x_count, tile_y_count FROM mmsi_layers WHERE layer_id = ?1 + 1;";

const char sql_read_tile[] = "SELECT data, mask FROM mmsi_tiles WHERE tile_id = ?1 + 1;";
const char sql_write_tile[] = "UPDATE mmsi_tiles SET data = ?2, mask = ?3, lazy_flag = ?4 WHERE tile_id = ?1 + 1;";
const char sql_insert_tile[] = "INSERT INTO mmsi_tiles (lazy_flag) VALUES (0);";
const char sql_query_tile[] = "SELECT lazy_flag FROM mmsi_tiles WHERE tile_id = ?1 + 1;";

namespace mmsi {
    
    class FilePrivate {
    public:
        std::string file;
        sqlite3 *db = nullptr;

        int32_t thread_num = 1;
        int32_t compress_mode = MMSIMODE_PNG;
        ThreadPool *pool;

        ImgCompressFunc ImgCompress = &ImgCompressPng;
        ImgUncompressFunc ImgUncompress = &ImgUncompressPng;
        
        sqlite3_stmt *read_prop = nullptr, *write_prop = nullptr, *insert_prop = nullptr, *delete_prop = nullptr, *count_prop = nullptr;
        sqlite3_stmt *insert_layer = nullptr, *query_layer = nullptr;
        sqlite3_stmt **read_tile, **write_tile;
        sqlite3_stmt *insert_tile = nullptr, *query_tile = nullptr;
        sqlite3_stmt *get_cache_size = nullptr, *set_cache_size = nullptr;
        
        int32_t version = MMSI_FILE_VERSION;
        int64_t width = 0, height = 0;
        int64_t tile_width = 0, tile_height = 0;
        int32_t level_cnt = 0;
        int32_t pixel_size = 0;
        
        std::vector<Layer*> layers;
        std::set<std::pair<int64_t, Tile*>> qcache;
        std::string error;
        
        int32_t cv_type = 0;
        int32_t comp_level = 3;
    };
    
    class Layer {
    public:
        Layer(File *mmsi, int32_t level);
        Layer(File *mmsi, int32_t level, int64_t start_id);
        virtual ~Layer();
        
        Tile *GetTile(int64_t x, int64_t y);
        
        Layer *GetUpper();
        Layer *GetLower();
        
        void ReadLevel(void *buf, int64_t x, int64_t y, int64_t w, int64_t h);
        void WriteLevel(void *buf, void *mask, int64_t x, int64_t y, int64_t w, int64_t h);
        
        File *m_mmsi;
        std::vector<Tile*> m_tiles;
        
        int32_t m_level;
        int64_t m_width, m_height;
        int64_t m_tiles_x, m_tiles_y;
        int64_t m_tile_start_id;
        int64_t m_downsample;
        
    };
    
    class Tile {
    public:
        Tile(Layer *layer, int64_t id);
        Tile(Layer *layer, int64_t id, int64_t x, int64_t y);
        virtual ~Tile();
        
        void ReadData(int thread);
        void WriteData(int thread);
        void AllocateMask();
        
        Tile *GetParent();
        Tile *GetChild(int32_t index);
        
        void Downward(int thread);
        void Upward(int thread);
        
        Layer *m_layer;
        int64_t m_cache_cnt = 0;
        
        bool m_lazy;
        bool m_modified;
        
        int64_t m_index;
        int64_t m_start_x, m_end_x;
        int64_t m_start_y, m_end_y;
        int64_t m_width, m_height;
        int64_t m_tile_x, m_tile_y;
        
        cv::Mat *m_data = nullptr;
        cv::Mat *m_mask = nullptr;
    };
    
    File::File(const std::string& filename) : m(new FilePrivate()) {
        m->file = filename;
    }
    
    File::~File() {
        delete m;
    }
    
    bool File::OpenFile() {
        m->pool = new ThreadPool(m->thread_num);

        try {
#ifdef SQLITE_PREPARE_PERSISTENT
            ERRCHK(sqlite3_prepare_v3(m->db, sql_read_prop, sizeof(sql_read_prop) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->read_prop, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_write_prop, sizeof(sql_write_prop) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->write_prop, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_insert_prop, sizeof(sql_insert_prop) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->insert_prop, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_delete_prop, sizeof(sql_delete_prop) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->delete_prop, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_count_prop, sizeof(sql_count_prop) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->count_prop, nullptr));

            ERRCHK(sqlite3_prepare_v3(m->db, sql_insert_layer, sizeof(sql_insert_layer) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->insert_layer, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_query_layer, sizeof(sql_query_layer) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->query_layer, nullptr));

            m->read_tile = new sqlite3_stmt*[m->thread_num];
            m->write_tile = new sqlite3_stmt*[m->thread_num];
            for (int i = 0; i < m->thread_num; i++) {
                ERRCHK(sqlite3_prepare_v3(m->db, sql_read_tile, sizeof(sql_read_tile) + 1,
                    SQLITE_PREPARE_PERSISTENT, &m->read_tile[i], nullptr));
                ERRCHK(sqlite3_prepare_v3(m->db, sql_write_tile, sizeof(sql_write_tile) + 1,
                    SQLITE_PREPARE_PERSISTENT, &m->write_tile[i], nullptr));
            }

            ERRCHK(sqlite3_prepare_v3(m->db, sql_insert_tile, sizeof(sql_insert_tile) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->insert_tile, nullptr));
            ERRCHK(sqlite3_prepare_v3(m->db, sql_query_tile, sizeof(sql_query_tile) + 1,
                SQLITE_PREPARE_PERSISTENT, &m->query_tile, nullptr));
#else
            ERRCHK(sqlite3_prepare_v2(m->db, sql_read_prop, sizeof(sql_read_prop) + 1,
                &m->read_prop, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_write_prop, sizeof(sql_write_prop) + 1,
                &m->write_prop, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_insert_prop, sizeof(sql_insert_prop) + 1,
                &m->insert_prop, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_delete_prop, sizeof(sql_delete_prop) + 1,
                &m->delete_prop, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_count_prop, sizeof(sql_count_prop) + 1,
                &m->count_prop, nullptr));

            ERRCHK(sqlite3_prepare_v2(m->db, sql_insert_layer, sizeof(sql_insert_layer) + 1,
                &m->insert_layer, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_query_layer, sizeof(sql_query_layer) + 1,
                &m->query_layer, nullptr));

            m->read_tile = new sqlite3_stmt * [m->thread_num];
            m->write_tile = new sqlite3_stmt * [m->thread_num];
            for (int i = 0; i < THREAD_CNT; i++) {
                ERRCHK(sqlite3_prepare_v2(m->db, sql_read_tile, sizeof(sql_read_tile) + 1,
                    &m->read_tile[i], nullptr));
                ERRCHK(sqlite3_prepare_v2(m->db, sql_write_tile, sizeof(sql_write_tile) + 1,
                    &m->write_tile[i], nullptr));
            }

            ERRCHK(sqlite3_prepare_v2(m->db, sql_insert_tile, sizeof(sql_insert_tile) + 1,
                &m->insert_tile, nullptr));
            ERRCHK(sqlite3_prepare_v2(m->db, sql_query_tile, sizeof(sql_query_tile) + 1,
                &m->query_tile, nullptr));
#endif
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
        
        return true;
    }
    
    bool File::CloseFile() {
        for (Layer *layer : m->layers)
            delete layer;
        
        try {
            ERRCHK(sqlite3_finalize(m->read_prop));
            ERRCHK(sqlite3_finalize(m->write_prop));
            ERRCHK(sqlite3_finalize(m->insert_prop));
            ERRCHK(sqlite3_finalize(m->delete_prop));
            ERRCHK(sqlite3_finalize(m->count_prop));

            ERRCHK(sqlite3_finalize(m->insert_layer));
            ERRCHK(sqlite3_finalize(m->query_layer));

            for (int i = 0; i < m->thread_num; i++) {
                ERRCHK(sqlite3_finalize(m->read_tile[i]));
                ERRCHK(sqlite3_finalize(m->write_tile[i]));
            }
            delete[] m->read_tile;
            delete[] m->write_tile;

            ERRCHK(sqlite3_finalize(m->insert_tile));
            ERRCHK(sqlite3_finalize(m->query_tile));

            ERRCHK(sqlite3_finalize(m->get_cache_size));
            ERRCHK(sqlite3_finalize(m->set_cache_size));

            ERRCHK(sqlite3_close(m->db));
        } catch (std::string &e) {
            m->error = e;
            return false;
        }

        delete m->pool;
        
        return true;
    }
    
    const std::string File::GetPropertyValue(const std::string &name) {
        try {
            ERRCHK(sqlite3_reset(m->read_prop));
            ERRCHK(sqlite3_bind_text(m->read_prop, 1, name.data(), -1, SQLITE_TRANSIENT));
            if (SQLITE_ROW == ERRCHK(sqlite3_step(m->read_prop)))
                return reinterpret_cast<const char*>(sqlite3_column_text(m->read_prop, 0));
            return "";
        } catch (std::string &e) {
            m->error = e;
            return "";
        }
    }
    
    void File::SetPropertyValue(const std::string &name, const std::string &value) {
        try {
            if (IsPropertyExisted(name)) {
                ERRCHK(sqlite3_reset(m->write_prop));
                ERRCHK(sqlite3_bind_text(m->write_prop, 1, name.data(), -1, SQLITE_TRANSIENT));
                ERRCHK(sqlite3_bind_text(m->write_prop, 2, value.data(), -1, SQLITE_TRANSIENT));
                ERRCHK(sqlite3_step(m->write_prop));
            } else {
                ERRCHK(sqlite3_reset(m->insert_prop));
                ERRCHK(sqlite3_bind_text(m->insert_prop, 1, name.data(), -1, SQLITE_TRANSIENT));
                ERRCHK(sqlite3_bind_text(m->insert_prop, 2, value.data(), -1, SQLITE_TRANSIENT));
                ERRCHK(sqlite3_step(m->insert_prop));
            }
        } catch (std::string &e) {
            m->error = e;
        }
    }
    
    void File::RemoveProperty(const std::string &name) {
        try {
            if (IsPropertyExisted(name)) {
                ERRCHK(sqlite3_reset(m->delete_prop));
                ERRCHK(sqlite3_bind_text(m->delete_prop, 1, name.data(), -1, SQLITE_TRANSIENT));
                ERRCHK(sqlite3_step(m->delete_prop));
            }
        } catch (std::string &e) {
            m->error = e;
        }
    }
    
    bool File::IsPropertyExisted(const std::string &name) {
        try {
            ERRCHK(sqlite3_reset(m->read_prop));
            ERRCHK(sqlite3_bind_text(m->read_prop, 1, name.data(), -1, SQLITE_TRANSIENT));
            return SQLITE_ROW == ERRCHK(sqlite3_step(m->read_prop));
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
    }
    
    const std::vector<std::string> File::GetPropertyNames() {
        try {
            std::vector<std::string> res;
            ERRCHK(sqlite3_reset(m->count_prop));
            while (SQLITE_ROW == ERRCHK(sqlite3_step(m->count_prop)))
                res.push_back(reinterpret_cast<const char*>(sqlite3_column_text(m->count_prop, 0)));
            return res;
        } catch (std::string &e) {
            m->error = e;
            return std::vector<std::string>();
        }
    }
    
    std::string File::GetDescription() {
        return GetPropertyValue(MMSIPROP_DESCRIPTION);
    }
        
    void File::SetDescription(const std::string &desc) {
        SetPropertyValue(MMSIPROP_DESCRIPTION, desc);
    }
    
    void File::SetCompressLevel(int32_t level) {
        m->comp_level = level;
    }

    void File::SetThreadNum(int32_t num) {
        m->thread_num = num;
    }

    void File::SetCompressMode(int32_t mode) {
        if (m->pixel_size != 4 && mode == MMSIMODE_JPG)
            mode = MMSIMODE_PNG;

        switch (mode) {
            case MMSIMODE_JPG: {
                m->ImgCompress = &ImgCompressJpg;
                m->ImgUncompress = &ImgUncompressJpg;
                break;
            }
            case MMSIMODE_ZLIB: {
                m->ImgCompress = &ImgCompressZlib;
                m->ImgUncompress = &ImgUncompressZlib;
                break;
            }
            default: {
                m->ImgCompress = &ImgCompressPng;
                m->ImgUncompress = &ImgUncompressPng;
                break;
            }
        }
    }
    
    bool File::HasError() {
        return m->error.length() != 0;
    }
    
    const std::string &File::GetError() {
        return m->error;
    }
    
    Writer::Writer(const std::string& filename) : File(filename) {
        m->version = MMSI_FILE_VERSION;
        m->width = m->height = -1;
        m->tile_width = m->tile_height = 256;
        m->level_cnt = -1;
        m->pixel_size = 4;
    }
    
    Writer::~Writer() {
        
    }
    
    bool Writer::OpenFile() {
        std::remove(m->file.data());
        
        try {
            ERRCHK(sqlite3_open_v2(m->file.data(), &m->db, SQLITE_OPEN_READWRITE |
                SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_SHAREDCACHE, nullptr));

            ERRCHK(sqlite3_exec(m->db, "PRAGMA auto_vacuum = FULL;", nullptr, nullptr, nullptr));
            ERRCHK(sqlite3_exec(m->db, "PRAGMA synchronous = OFF;", nullptr, nullptr, nullptr));
            ERRCHK(sqlite3_exec(m->db, "PRAGMA temp_store = MEMORY;", nullptr, nullptr, nullptr));
            ERRCHK(sqlite3_exec(m->db, "PRAGMA journal_mode = OFF;", nullptr, nullptr, nullptr));

            ERRCHK(sqlite3_exec(m->db, sql_create_prop_table, nullptr, nullptr, nullptr));
            ERRCHK(sqlite3_exec(m->db, sql_create_layer_table, nullptr, nullptr, nullptr));
            ERRCHK(sqlite3_exec(m->db, sql_create_tile_table, nullptr, nullptr, nullptr));
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
        
        switch (m->pixel_size) {
            case 1: m->cv_type = CV_8U; break;
            case 2: m->cv_type = CV_16U; break;
            case 4: m->cv_type = CV_8UC4; break;
            case 8: m->cv_type = CV_16UC4; break;
            default: return false;
        }
        
        if (!File::OpenFile()) return false;
        InitLayer(0);
        
        return true;
    }
        
    void Writer::InitRegion(void *data, void *mask, int64_t x, int64_t y, int64_t w, int64_t h) {
        cv::Mat input(h, w, m->cv_type, data), imask;
        int64_t tl = x / m->tile_width, tt = y / m->tile_height;
        int64_t tr = (x + w - 1) / m->tile_width, tb = (y + h - 1) / m->tile_height;
        if (mask != nullptr)
            imask = cv::Mat(h, w, CV_8U, mask);
        
        int64_t zero = 0;
        for (int64_t tx = tl; tx <= tr; tx++) {
            for (int64_t ty = tt; ty <= tb; ty++) {
                Tile *tile = m->layers[0]->GetTile(tx, ty);
                tile->ReadData(0);
                
                int64_t il = std::max(zero, tx * m->tile_width - x);
                int64_t ir = std::min(w, (tx + 1) * m->tile_width - x);
                int64_t it = std::max(zero, ty * m->tile_height - y);
                int64_t ib = std::min(h, (ty + 1) * m->tile_height - y);
                
                int64_t dl = std::max(zero, x - tx * m->tile_width);
                int64_t dr = std::min(m->tile_width, x + w - tx * m->tile_width);
                int64_t dt = std::max(zero, y - ty * m->tile_height);
                int64_t db = std::min(m->tile_height, y + h - ty * m->tile_height);
                
                cv::Mat iroi = input(cv::Range(it, ib), cv::Range(il, ir));
                cv::Mat droi = (*tile->m_data)(cv::Range(dt, db), cv::Range(dl, dr));
                if (mask != nullptr) {
                    cv::Mat mroi = imask(cv::Range(it, ib), cv::Range(il, ir));
                    iroi.copyTo(droi, mroi);
                } else iroi.copyTo(droi);
                tile->m_modified = true;
            }
        }
    }
    
    bool Writer::CloseFile() {
        if (m->level_cnt < 0) {
            int64_t width = m->width;
            int64_t height = m->height;
            m->level_cnt = 1;
            while (width > m->tile_width || height > m->tile_height) {
                width >>= 1;
                height >>= 1;
                m->level_cnt++;
            }
        }
        
        try {
            Layer *lowlayer = m->layers[0];
            for (int32_t l = 1; l < m->level_cnt; l++) {
                Layer *layer = InitLayer(l);
                for (Tile *curtile : lowlayer->m_tiles) {
                    if (curtile->m_data == nullptr)
                        continue;
                    curtile->Upward(0);
                }

                for (Tile *tile : lowlayer->m_tiles)
                    tile->WriteData(0);
                lowlayer = layer;
            }

            for (Tile *tile : lowlayer->m_tiles)
                tile->WriteData(0);
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
        
        SetPropertyValue(MMSIPROP_VERSION, std::to_string(m->version));
        SetPropertyValue(MMSIPROP_IMAGE_WIDTH, std::to_string(m->width));
        SetPropertyValue(MMSIPROP_IMAGE_HEIGHT, std::to_string(m->height));
        SetPropertyValue(MMSIPROP_TILE_WIDTH, std::to_string(m->tile_width));
        SetPropertyValue(MMSIPROP_TILE_HEIGHT, std::to_string(m->tile_height));
        SetPropertyValue(MMSIPROP_LEVEL_COUNT, std::to_string(m->level_cnt));
        SetPropertyValue(MMSIPROP_PIXEL_SIZE, std::to_string(m->pixel_size));
        if (!IsPropertyExisted(MMSIPROP_DESCRIPTION))
            SetPropertyValue(MMSIPROP_DESCRIPTION, "");
        
        if (!File::CloseFile()) return false;
        
        return true;
    }
        
    void Writer::SetImageSize(int64_t width, int64_t height) {
        m->width = width, m->height = height;
    }
    
    void Writer::SetTileSize(int64_t width, int64_t height) {
        m->tile_width = width, m->tile_height = height;
    }
    
    void Writer::SetPixelSize(int32_t size) {
        m->pixel_size = size;
    }
    
    void Writer::SetLevelCount(int32_t count) {
        m->level_cnt = count;
    }
    
    Layer *Writer::InitLayer(int32_t level) {
        Layer *layer = new Layer(this, level, m_tile_cnt);
        m->layers.push_back(layer);
        m_tile_cnt += layer->m_tiles.size();
        return layer;
    }
    
    Accessor::Accessor(const std::string& filename) : File(filename) {
        m_cache_size = 0;
        m_secure_mode = false;
    }
    
    Accessor::~Accessor() {
        
    }
        
    bool Accessor::OpenFile() {
        try {
            ERRCHK(sqlite3_open_v2(m->file.data(), &m->db, SQLITE_OPEN_READWRITE | 
                SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_SHAREDCACHE, nullptr));
        
            if (!File::OpenFile()) return false;

            m->version = std::stoi(GetPropertyValue(MMSIPROP_VERSION));
            if (m->version > MMSI_FILE_VERSION)
                throw std::string("This file requires a higher version of libmmsi.");
            m->width = std::stol(GetPropertyValue(MMSIPROP_IMAGE_WIDTH));
            m->height = std::stol(GetPropertyValue(MMSIPROP_IMAGE_HEIGHT));
            m->tile_width = std::stol(GetPropertyValue(MMSIPROP_TILE_WIDTH));
            m->tile_height = std::stol(GetPropertyValue(MMSIPROP_TILE_HEIGHT));
            m->level_cnt = std::stoi(GetPropertyValue(MMSIPROP_LEVEL_COUNT));
            m->pixel_size = std::stoi(GetPropertyValue(MMSIPROP_PIXEL_SIZE));

            switch (m->pixel_size) {
                case 1: m->cv_type = CV_8U; break;
                case 2: m->cv_type = CV_16U; break;
                case 4: m->cv_type = CV_8UC4; break;
                case 8: m->cv_type = CV_16UC4; break;
                default: return false;
            }

            for (int32_t i = 0; i < m->level_cnt; i++)
                m->layers.push_back(new Layer(this, i));
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
        
        return true;
    }
    
    bool Accessor::CloseFile() {
        try {
            m_roi.x = m_roi.y = 0;
            m_roi.w = m->width, m_roi.h = m->height;
            Upward(m->level_cnt - 1);

            m_cache_size = 0;
            ClearCache(true);
        } catch (std::string &e) {
            m->error = e;
            return false;
        }
        
        if (!File::CloseFile()) return false;
        
        return true;
    }
    
    void Accessor::SetCurrentRegion(int64_t x, int64_t y, int64_t w, int64_t h) {
        m_roi.x = x; m_roi.y = y; m_roi.w = w; m_roi.h = h;
    }
    
    int64_t Accessor::GetRegionWidthLevel(int32_t level) {
        return m_roi.w / GetDownsample(level);
    }
    
    int64_t Accessor::GetRegionHeightLevel(int32_t level) {
        return m_roi.h / GetDownsample(level);
    }

    void Accessor::ReadRegion(int32_t level, void *buf) {
        try {
            Upward(level);
            Downward(level);
            m->layers[level]->ReadLevel(buf, m_roi.x, m_roi.y, m_roi.w, m_roi.h);
            ClearCache(true);
        } catch (std::string &e) {
            m->error = e;
        }
    }
    
    void Accessor::WriteRegion(int32_t level, void *buf, void *mask) {
        try {
            Upward(level);
            Downward(level);

            cv::Mat mmask;
            if (mask == nullptr) {
                int64_t ds = 1 << level;
                mmask = cv::Mat(m_roi.h / ds, m_roi.w / ds, CV_8U);
                memset(mmask.data, 255, mmask.total());
                mask = mmask.data;
            }

            m->layers[level]->WriteLevel(buf, mask, m_roi.x, m_roi.y, m_roi.w, m_roi.h);
            ClearCache(true);
        } catch (std::string &e) {
            m->error = e;
        }
    }
    
    void Accessor::Downward(int32_t low) {
        for (int32_t i = m->level_cnt - 1; i > low; i--) {
            Layer *layer = m->layers[i];
            
            int64_t il = m_roi.x / layer->m_downsample;
            int64_t ir = (m_roi.x + m_roi.w) / layer->m_downsample;
            int64_t it = m_roi.y / layer->m_downsample;
            int64_t ib = (m_roi.y + m_roi.h) / layer->m_downsample;
            
            int64_t tl = il / m->tile_width, tt = it / m->tile_height;
            int64_t tr = (ir - 1) / m->tile_width, tb = (ib - 1) / m->tile_height;
            
            for (int64_t tx = tl; tx <= tr; tx++) {
                for (int64_t ty = tt; ty <= tb; ty++) {
                    m->pool->AddJob([layer, tx, ty](int i) {
                        layer->GetTile(tx, ty)->Downward(i);
                        });
                    
                    ClearCache(false);
                }
            }

            m->pool->WaitAll();
        }
    }
    
    void Accessor::Upward(int32_t high) {
        if (m_secure_mode)
            high = m->level_cnt - 1;
        
        int64_t ds = 1 << high;
            
        int64_t il = m_roi.x / ds;
        int64_t ir = (m_roi.x + m_roi.w) / ds;
        int64_t it = m_roi.y / ds;
        int64_t ib = (m_roi.y + m_roi.h) / ds;

        int64_t tl = il / m->tile_width, tt = it / m->tile_height;
        int64_t tr = (ir - 1) / m->tile_width, tb = (ib - 1) / m->tile_height;
        
        auto iter = m_mod_tiles.begin();
        for (int32_t i = 1; i <= high && iter != m_mod_tiles.end(); i++) {
            if (iter->first == i) {
                Layer *layer = m->layers[i];
                
                int64_t lds = 1 << (high - i);
                
                while (iter != m_mod_tiles.end() && iter->first == i) {
                    Tile *tile = iter->second;
                    if (tile->m_tile_x / lds >= tl && tile->m_tile_x / lds <= tr && tile->m_tile_y / lds >= tt && tile->m_tile_y / lds <= tb) {
                        if (i + 1 < m->level_cnt)
                            m_mod_tiles.insert({ i + 1, tile->GetParent() });
                        iter = m_mod_tiles.erase(iter);
                        m->pool->AddJob([tile, this, i](int thread) {
                            for (int j = 0; j < 4; j++) {
                                Layer* layer = m->layers[i - 1];
                                if (tile->m_tile_x * 2 + j / 2 < layer->m_tiles_x
                                    && tile->m_tile_y * 2 + j % 2 < layer->m_tiles_y)
                                    tile->GetChild(j)->Upward(thread);
                            }
                            });
                        ClearCache(false);
                    } else iter++;
                }

                m->pool->WaitAll();
            }
        }
    }
    
    int64_t Accessor::GetImageWidth() {
        return m->width;
    }
    
    int64_t Accessor::GetImageHeight() {
        return m->height;
    }
    
    int64_t Accessor::GetTileWidth() {
        return m->tile_width;
    }
    
    int64_t Accessor::GetTileHeight() {
        return m->tile_height;
    }
    
    int32_t Accessor::GetPixelSize() {
        return m->pixel_size;
    }
    
    int32_t Accessor::GetLevelCount() {
        return m->level_cnt;
    }
    
    int32_t Accessor::GetCacheSize() {
        return m_cache_size;
    }

    void Accessor::SetCacheSize(int32_t size) {
        m_cache_size = size;
    }
    
    void Accessor::SetSecureMode(bool enable) {
        m_secure_mode = enable;
    }
    
    int64_t Accessor::GetDownsample(int32_t level) {
        return 1L << level;
    }
    
    void Accessor::ClearCache(bool force) {
        int32_t csize = m_secure_mode ? 0 : m_cache_size;
        if (m->qcache.size() > csize * (force ? 1 : 1.5)) {
            m->pool->WaitAll();

            if (csize > 0) {
                for (auto iter = m->qcache.begin(); iter != m->qcache.end() && m->qcache.size() > csize;) {
                    if (!iter->second->m_modified) {
                        iter->second->WriteData(0);
                        iter = m->qcache.erase(iter);
                    } else iter++;
                }
            }
            
            int cnt = m->qcache.size() - csize, i = 0;
            for (auto iter = m->qcache.begin(); i++ < cnt;) {
                Tile* tile = iter->second;
                m->pool->AddJob([tile](int i) {
                    tile->WriteData(i);
                    });
                iter = m->qcache.erase(iter);
            }

            m->pool->WaitAll();
        }
    }
    
    Layer::Layer(File* mmsi, int32_t level) {
        m_mmsi = mmsi;
        m_level = level;
        m_downsample = 1 << level;
        
        sqlite3_stmt *qs = mmsi->m->query_layer;
        ERRCHK(sqlite3_reset(qs));
        ERRCHK(sqlite3_bind_int(qs, 1, level));
        ERRCHK(sqlite3_step(qs));
        m_width = sqlite3_column_int64(qs, 0);
        m_height = sqlite3_column_int64(qs, 1);
        m_tile_start_id = sqlite3_column_int64(qs, 2);
        m_tiles_x = sqlite3_column_int64(qs, 3);
        m_tiles_y = sqlite3_column_int64(qs, 4);
        
        int64_t tile_cnt = m_tiles_x * m_tiles_y;
        for (int64_t i = 0; i < tile_cnt; i++)
            m_tiles.push_back(new Tile(this, m_tile_start_id + i));
    }
    
    Layer::Layer(File* mmsi, int32_t level, int64_t start_id) {
        m_mmsi = mmsi;
        m_level = level;
        m_downsample = 1 << level;
        
        m_width = (mmsi->m->width - 1) / m_downsample + 1;
        m_height = (mmsi->m->height - 1) / m_downsample + 1;
        m_tile_start_id = start_id;
        m_tiles_x = (m_width - 1) / mmsi->m->tile_width + 1;
        m_tiles_y = (m_height - 1) / mmsi->m->tile_height + 1;
        
        sqlite3_stmt *is = mmsi->m->insert_layer;
        ERRCHK(sqlite3_reset(is));
        ERRCHK(sqlite3_bind_int64(is, 1, m_width));
        ERRCHK(sqlite3_bind_int64(is, 2, m_height));
        ERRCHK(sqlite3_bind_int64(is, 3, m_tile_start_id));
        ERRCHK(sqlite3_bind_int64(is, 4, m_tiles_x));
        ERRCHK(sqlite3_bind_int64(is, 5, m_tiles_y));
        ERRCHK(sqlite3_step(is));
        
        int64_t tile_cnt = m_tiles_x * m_tiles_y;
        for (int64_t i = 0; i < tile_cnt; i++)
            m_tiles.push_back(new Tile(this, m_tile_start_id + i, i / m_tiles_y, i % m_tiles_y));
    }
    
    Layer::~Layer() {
        for (Tile *tile : m_tiles)
            delete tile;
    }
        
    Tile *Layer::GetTile(int64_t x, int64_t y) {
        return m_tiles[x * m_tiles_y + y];
    }

    Layer *Layer::GetUpper() {
        return m_mmsi->m->layers[m_level + 1];
    }

    Layer *Layer::GetLower() {
        return m_mmsi->m->layers[m_level - 1];
    }
    
    void Layer::ReadLevel(void *data, int64_t x, int64_t y, int64_t w, int64_t h) {
        FilePrivate *fp = m_mmsi->m;
        
        x /= m_downsample; w /= m_downsample;
        y /= m_downsample; h /= m_downsample;
        cv::Mat input(h, w, fp->cv_type, data);
        int64_t tl = x / fp->tile_width, tt = y / fp->tile_height;
        int64_t tr = (x + w - 1) / fp->tile_width, tb = (y + h - 1) / fp->tile_height;
        
        for (int64_t tx = tl; tx <= tr; tx++) {
            for (int64_t ty = tt; ty <= tb; ty++) {
                m_mmsi->m->pool->AddJob([this, tx, ty, fp, x, y, w, h, &input](int i) {
                    int64_t zero = 0;
                    Tile* tile = GetTile(tx, ty);
                    tile->ReadData(i);

                    int64_t il = std::max(zero, tx * fp->tile_width - x);
                    int64_t ir = std::min(w, (tx + 1) * fp->tile_width - x);
                    int64_t it = std::max(zero, ty * fp->tile_height - y);
                    int64_t ib = std::min(h, (ty + 1) * fp->tile_height - y);

                    int64_t dl = std::max(zero, x - tx * fp->tile_width);
                    int64_t dr = std::min(fp->tile_width, x + w - tx * fp->tile_width);
                    int64_t dt = std::max(zero, y - ty * fp->tile_height);
                    int64_t db = std::min(fp->tile_height, y + h - ty * fp->tile_height);

                    cv::Mat iroi = input(cv::Range(it, ib), cv::Range(il, ir));
                    cv::Mat droi = (*tile->m_data)(cv::Range(dt, db), cv::Range(dl, dr));
                    droi.copyTo(iroi);
                    });
                static_cast<Accessor*>(m_mmsi)->ClearCache(false);
            }
        }

        m_mmsi->m->pool->WaitAll();
    }
    
    void Layer::WriteLevel(void *data, void *mask, int64_t x, int64_t y, int64_t w, int64_t h) {
        FilePrivate *fp = m_mmsi->m;
            
        x /= m_downsample; w /= m_downsample;
        y /= m_downsample; h /= m_downsample;
        cv::Mat input(h, w, fp->cv_type, data), imask;
        int64_t tl = x / fp->tile_width, tt = y / fp->tile_height;
        int64_t tr = (x + w - 1) / fp->tile_width, tb = (y + h - 1) / fp->tile_height;
        if (mask != nullptr)
            imask = cv::Mat(h, w, CV_8U, mask);
        
        for (int64_t tx = tl; tx <= tr; tx++) {
            for (int64_t ty = tt; ty <= tb; ty++) {
                Tile *tile = GetTile(tx, ty);
                m_mmsi->m->pool->AddJob([this, tile, tx, ty, fp, x, y, w, h, &input, mask, &imask](int i) {
                    int64_t zero = 0;
                    tile->ReadData(i);

                    int64_t il = std::max(zero, tx * fp->tile_width - x);
                    int64_t ir = std::min(w, (tx + 1) * fp->tile_width - x);
                    int64_t it = std::max(zero, ty * fp->tile_height - y);
                    int64_t ib = std::min(h, (ty + 1) * fp->tile_height - y);

                    int64_t dl = std::max(zero, x - tx * fp->tile_width);
                    int64_t dr = std::min(fp->tile_width, x + w - tx * fp->tile_width);
                    int64_t dt = std::max(zero, y - ty * fp->tile_height);
                    int64_t db = std::min(fp->tile_height, y + h - ty * fp->tile_height);

                    cv::Mat iroi = input(cv::Range(it, ib), cv::Range(il, ir));
                    cv::Mat droi = (*tile->m_data)(cv::Range(dt, db), cv::Range(dl, dr));
                    if (mask != nullptr) {
                        cv::Mat mroi = imask(cv::Range(it, ib), cv::Range(il, ir));
                        iroi.copyTo(droi, mroi);
                        if (m_level > 0) {
                            tile->AllocateMask();
                            cv::Mat dmroi = (*tile->m_mask)(cv::Range(dt, db), cv::Range(dl, dr));
                            mroi.copyTo(dmroi, mroi);
                        }
                    }
                    else iroi.copyTo(droi);
                    tile->m_modified = true;
                    if (m_level > 0)
                        tile->m_lazy = true;
                    });
                if (m_level < m_mmsi->m->level_cnt - 1)
                    static_cast<Accessor*>(m_mmsi)->m_mod_tiles.insert({ m_level + 1, tile->GetParent() });
            }
        }

        m_mmsi->m->pool->WaitAll();
    }
    
    Tile::Tile(Layer* layer, int64_t id) {
        m_layer = layer;
        m_index = id;
        
        sqlite3_stmt *qs = layer->m_mmsi->m->query_tile;
        ERRCHK(sqlite3_reset(qs));
        ERRCHK(sqlite3_bind_int64(qs, 1, id));
        ERRCHK(sqlite3_step(qs));
        m_lazy = sqlite3_column_int(qs, 0);
        
        int64_t index = id - layer->m_tile_start_id;
        m_tile_x = index / layer->m_tiles_y;
        m_tile_y = index % layer->m_tiles_y;
        m_start_x = layer->m_mmsi->m->tile_width;
        m_start_y = layer->m_mmsi->m->tile_height;
        m_end_x = std::min(m_start_x * (m_tile_x + 1), layer->m_width);
        m_end_y = std::min(m_start_y * (m_tile_y + 1), layer->m_height);
        m_start_x *= m_tile_x;
        m_start_y *= m_tile_y;
        
        m_width = m_end_x - m_start_x;
        m_height = m_end_y - m_start_y;
    }
    
    Tile::Tile(Layer* layer, int64_t id, int64_t x, int64_t y) {
        m_layer = layer;
        m_index = id;
        
        m_start_x = layer->m_mmsi->m->tile_width;
        m_start_y = layer->m_mmsi->m->tile_height;
        m_end_x = std::min(m_start_x * (x + 1), layer->m_width);
        m_end_y = std::min(m_start_y * (y + 1), layer->m_height);
        m_start_x *= x;
        m_start_y *= y;
        
        m_width = m_end_x - m_start_x;
        m_height = m_end_y - m_start_y;
        m_tile_x = x;
        m_tile_y = y;
        m_lazy = false;
        
        sqlite3_stmt *is = layer->m_mmsi->m->insert_tile;
        ERRCHK(sqlite3_reset(is));
        ERRCHK(sqlite3_step(is));
    }
    
    Tile::~Tile() {
        
    }
        
    void Tile::ReadData(int thread) {
        m_cache_cnt++;
        if (m_data != nullptr)
            return;
        
        m_modified = false;
        FilePrivate *fp = m_layer->m_mmsi->m;
        sqlite3_stmt* rs = fp->read_tile[thread];
        {
            std::unique_lock<std::mutex> lk(g_mutex);
            fp->qcache.insert({ m_cache_cnt, this });
            ERRCHK(sqlite3_reset(rs));
            ERRCHK(sqlite3_bind_int64(rs, 1, m_index));
            ERRCHK(sqlite3_step(rs));
        }
        
        uint64_t datalen = sqlite3_column_bytes(rs, 0);
        m_data = new cv::Mat(fp->tile_height, fp->tile_width, fp->cv_type);
        uint64_t odatalen = m_data->total() * fp->pixel_size;
        if (datalen > 0) {
            m_layer->m_mmsi->m->ImgUncompress(m_data, sqlite3_column_blob(rs, 0), datalen, odatalen, fp->pixel_size);
        } else memset(m_data->data, 0, odatalen);
        
        uint64_t masklen = sqlite3_column_bytes(rs, 1);
        if (masklen > 0) {
            m_mask = new cv::Mat(fp->tile_height, fp->tile_width, CV_8U);
            uint64_t omasklen = m_data->total();
            m_layer->m_mmsi->m->ImgUncompress(m_mask, sqlite3_column_blob(rs, 1), masklen, omasklen, 1);
        }
    }
    
    void Tile::WriteData(int thread) {
        if (m_data == nullptr)
            return;
        
        if (m_modified) {
            FilePrivate *fp = m_layer->m_mmsi->m;
            sqlite3_stmt *ws = fp->write_tile[thread];
            ERRCHK(sqlite3_bind_int64(ws, 1, m_index));

            auto buf = m_layer->m_mmsi->m->ImgCompress(m_data, fp->pixel_size, fp->comp_level);
            ERRCHK(sqlite3_bind_blob(ws, 2, buf.first, buf.second, std::free));

            if (m_mask != nullptr) {
                auto buf = m_layer->m_mmsi->m->ImgCompress(m_mask, 1, fp->comp_level);
                ERRCHK(sqlite3_bind_blob(ws, 3, buf.first, buf.second, std::free));
            } else ERRCHK(sqlite3_bind_null(ws, 3));

            ERRCHK(sqlite3_bind_int(ws, 4, m_lazy));
            {
                std::unique_lock<std::mutex> lk(g_mutex);
                ERRCHK(sqlite3_step(ws));
                ERRCHK(sqlite3_reset(ws));
            }
        }
        
        delete m_data;
        m_data = nullptr;
        if (m_mask != nullptr) {
            delete m_mask;
            m_mask = nullptr;
        }
    }
    
    void Tile::AllocateMask() {
        if (m_mask == nullptr) {
            FilePrivate *fp = m_layer->m_mmsi->m;
            m_mask = new cv::Mat(fp->tile_height, fp->tile_width, CV_8U);
            memset(m_mask->data, 0, m_mask->total());
        }
    }

    Tile *Tile::GetParent() {
        return m_layer->GetUpper()->GetTile(m_tile_x / 2, m_tile_y / 2);
    }
    
    Tile *Tile::GetChild(int32_t index) {
        return m_layer->GetLower()->GetTile(m_tile_x * 2 + index / 2, m_tile_y * 2 + index % 2);
    }
        
    void Tile::Downward(int thread) {
        if (m_lazy) {
            ReadData(thread);
            m_lazy = false;
            FilePrivate *fp = m_layer->m_mmsi->m;
            Layer *lowlayer = m_layer->GetLower();
            for (int subid = 0; subid < 4; subid++) {
                int64_t subx = m_tile_x * 2 + subid / 2;
                int64_t suby = m_tile_y * 2 + subid % 2;
                if (subx < lowlayer->m_tiles_x && suby < lowlayer->m_tiles_y) {
                    Tile *subtile = lowlayer->GetTile(subx, suby);
                    subtile->ReadData(thread);
                    cv::Mat droi = *subtile->m_data;
                    cv::Mat sroi = (*m_data)(cv::Range(subid % 2 * fp->tile_height / 2, (subid % 2 + 1) * fp->tile_height / 2),
                            cv::Range(subid / 2 * fp->tile_width / 2, (subid / 2 + 1) * fp->tile_width / 2));
                    
                    if (m_mask != nullptr) {
                        cv::Mat mroi = (*m_mask)(cv::Range(subid % 2 * fp->tile_height / 2, (subid % 2 + 1) * fp->tile_height / 2),
                                cv::Range(subid / 2 * fp->tile_width / 2, (subid / 2 + 1) * fp->tile_width / 2));
                        cv::Mat smat, mmat;
                        cv::resize(sroi, smat, cv::Size(fp->tile_width, fp->tile_height), 0, 0, cv::INTER_NEAREST);
                        cv::resize(mroi, mmat, cv::Size(fp->tile_width, fp->tile_height), 0, 0, cv::INTER_NEAREST);
                        smat.copyTo(droi, mmat);
                        if (lowlayer->m_level > 0) {
                            subtile->AllocateMask();
                            cv::Mat dmroi = *subtile->m_mask;
                            mmat.copyTo(dmroi, mmat);
                        }
                    } else {
                        cv::resize(sroi, droi, cv::Size(fp->tile_width, fp->tile_height), 0, 0, cv::INTER_NEAREST);
                    }
                    
                    subtile->m_modified = true;
                    if (lowlayer->m_level > 0)
                        subtile->m_lazy = true;
                }
            }
                  
            if (m_mask != nullptr) {      
                delete m_mask;
                m_mask = nullptr;
                m_modified = true;
            }
        }
    }
    
    void Tile::Upward(int thread) {
        ReadData(thread);
        FilePrivate *fp = m_layer->m_mmsi->m;
        Tile *partile = GetParent();
        partile->ReadData(thread);
        
        cv::Mat sroi = *m_data;
        cv::Mat droi = (*partile->m_data)(cv::Range(m_tile_y % 2 * fp->tile_height / 2, (m_tile_y % 2 + 1) * fp->tile_height / 2),
                cv::Range(m_tile_x % 2 * fp->tile_width / 2, (m_tile_x % 2 + 1) * fp->tile_width / 2));
        cv::resize(sroi, droi, cv::Size(fp->tile_width / 2, fp->tile_height / 2), 0, 0, cv::INTER_NEAREST);
        partile->m_modified = true;
    }
    
    struct MMSI_t {
        File *file;
        Writer *writer;
        Accessor *accessor;
        
        std::string prop_temp;
        std::vector<std::string> prop_names;
        char **c_prop_names = nullptr;
        
        MMSI_t(Writer *mmsi) {
            this->file = mmsi;
            this->writer = mmsi;
            this->accessor = nullptr;
        }
        
        MMSI_t(Accessor *mmsi) {
            this->file = mmsi;
            this->accessor = mmsi;
            this->writer = nullptr;
        }
        
        ~MMSI_t() {
            if (c_prop_names)
                delete[] c_prop_names;
            delete this->file;
        }
    };

    const char *mmsi_Version() {
        return MMSI_VERSION;
    }

    MMSI *mmsi_Writer(const char *filename) {
        return new MMSI(new Writer(filename));
    }
    
    MMSI *mmsi_Accessor(const char *filename) {
        return new MMSI(new Accessor(filename));
    }
    
    void mmsi_Destroy(MMSI *mmsi) {
        delete mmsi;
    }

    bool mmsi_Open(MMSI *mmsi) {
        return mmsi->file->OpenFile();
    }
    
    bool mmsi_Close(MMSI *mmsi) {
        return mmsi->file->CloseFile();
    }

    const char *mmsi_GetPropertyValue(MMSI *mmsi, const char *key) {
        mmsi->prop_temp = mmsi->file->GetPropertyValue(key);
        return mmsi->prop_temp.data();
    }
    
    void mmsi_SetPropertyValue(MMSI *mmsi, const char *key, const char *value) {
        mmsi->file->SetPropertyValue(key, value);
    }
    
    void mmsi_RemoveProperty(MMSI *mmsi, const char *key) {
        mmsi->file->RemoveProperty(key);
    }
    
    bool mmsi_IsPropertyExisted(MMSI *mmsi, const char *key) {
        return mmsi->file->IsPropertyExisted(key);
    }
    
    const char *const *mmsi_GetPropertyNames(MMSI *mmsi) {
        mmsi->prop_names = mmsi->file->GetPropertyNames();
        if (mmsi->c_prop_names) delete[] mmsi->c_prop_names;
        mmsi->c_prop_names = new char*[mmsi->prop_names.size() + 1];
        
        int32_t i = 0;
        for ( ; i < mmsi->prop_names.size(); i++)
            mmsi->c_prop_names[i] = const_cast<char*>(mmsi->prop_names[i].data());
        mmsi->c_prop_names[i] = nullptr;
        return mmsi->c_prop_names;
    }

    const char *mmsi_GetDescription(MMSI *mmsi) {
        return mmsi_GetPropertyValue(mmsi, MMSIPROP_DESCRIPTION);
    }
    
    void mmsi_SetDescription(MMSI *mmsi, const char *desc) {
        mmsi->file->SetDescription(desc);
    }
    
    bool mmsi_HasError(MMSI *mmsi) {
        return mmsi->file->HasError();
    }
    
    const char *mmsi_GetError(MMSI *mmsi) {
        return mmsi->file->GetError().data();
    }
    
    void mmsi_SetCompressLevel(MMSI *mmsi, int32_t level) {
        mmsi->file->SetCompressLevel(level);
    }

    void mmsi_SetThreadNum(MMSI* mmsi, int32_t num) {
        mmsi->file->SetThreadNum(num);
    }

    void mmsi_SetCompressMode(MMSI* mmsi, int32_t mode) {
        mmsi->file->SetCompressMode(mode);
    }

    void mmsi_SetImageSize(MMSI *mmsi, int64_t width, int64_t height) {
        mmsi->writer->SetImageSize(width, height);
    }
    
    void mmsi_SetTileSize(MMSI *mmsi, int64_t width, int64_t height) {
        mmsi->writer->SetTileSize(width, height);
    }
    
    void mmsi_SetPixelSize(MMSI *mmsi, int32_t size) {
        mmsi->writer->SetPixelSize(size);
    }
    
    void mmsi_SetLevelCount(MMSI *mmsi, int32_t count) {
        mmsi->writer->SetLevelCount(count);
    }

    void mmsi_InitRegion(MMSI *mmsi, void *data, void *mask, int64_t x, int64_t y, int64_t w, int64_t h) {
        mmsi->writer->InitRegion(data, mask, x, y, w, h);
    }

    void mmsi_SetCurrentRegion(MMSI *mmsi, int64_t x, int64_t y, int64_t w, int64_t h) {
        mmsi->accessor->SetCurrentRegion(x, y, w, h);
    }

    void mmsi_ReadRegion(MMSI *mmsi, int32_t level, void *buf) {
        mmsi->accessor->ReadRegion(level, buf);
    }
    
    void mmsi_WriteRegion(MMSI *mmsi, int32_t level, void *buf, void *mask) {
        mmsi->accessor->WriteRegion(level, buf, mask);
    }

    int64_t mmsi_GetImageWidth(MMSI *mmsi) {
        return mmsi->accessor->GetImageWidth();
    }
    
    int64_t mmsi_GetImageHeight(MMSI *mmsi) {
        return mmsi->accessor->GetImageHeight();
    }
    
    int64_t mmsi_GetTileWidth(MMSI *mmsi) {
        return mmsi->accessor->GetTileWidth();
    }
    
    int64_t mmsi_GetTileHeight(MMSI *mmsi) {
        return mmsi->accessor->GetTileHeight();
    }
    
    int32_t mmsi_GetPixelSize(MMSI *mmsi) {
        return mmsi->accessor->GetPixelSize();
    }
    
    int32_t mmsi_GetLevelCount(MMSI *mmsi) {
        return mmsi->accessor->GetLevelCount();
    }
    
    int64_t mmsi_GetDownsample(MMSI *mmsi, int32_t level) {
        return mmsi->accessor->GetDownsample(level);
    }

    int32_t mmsi_GetCacheSize(MMSI *mmsi) {
        return mmsi->accessor->GetCacheSize();
    }
    
    void mmsi_SetCacheSize(MMSI *mmsi, int32_t size) {
        return mmsi->accessor->SetCacheSize(size);
    }
    
    int64_t mmsi_GetRegionWidthLevel(MMSI *mmsi, int32_t level) {
        return mmsi->accessor->GetRegionWidthLevel(level);
    }
    
    int64_t mmsi_GetRegionHeightLevel(MMSI *mmsi, int32_t level) {
        return mmsi->accessor->GetRegionHeightLevel(level);
    }
    
    void mmsi_SetSecureMode(MMSI *mmsi, bool enable) {
        mmsi->accessor->SetSecureMode(enable);
    }
    
}