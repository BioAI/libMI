/*
 * Copyright (C) 2018 GPIAY
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   libmi.cpp
 * Author: GPIAY
 * 
 * Created on November 5, 2018, 6:50 PM
 */

#include <fstream>
#include <sstream>
#include <memory>

#include <opencv2/opencv.hpp>
#include <openslide/openslide.h>
#include <sqlite3.h>
#include <zlib.h>

#if (defined(__APPLE__) && defined(__MACH__)) || defined(WIN32)
#include <json/json.h>
#else
#include <jsoncpp/json/json.h>
#endif

#include "mmsi.h"
#include "libmi.h"

#include "../../config.h"

const char sql_read_grade[] = "SELECT data FROM openhil_grade WHERE tile_id = ?1;";
const char sql_write_grade[] = "UPDATE openhil_grade SET data = ?2 WHERE tile_id = ?1;";
const char sql_insert_grade[] = "INSERT INTO openhil_grade VALUES (?1, ?2);";

#define OPENHIL_VERSION         VERSION
#define OPENHIL_FILE_VERSION    FILE_VERSION

namespace ohil {
    
    class AnnoPrivate;
    
    class Slide::SlidePrivate {
    public:
        
        SlideType type = SlideType::UNKNOWN;

        openslide_t *openslide = nullptr;

        std::vector<Anno*> annos;
        std::vector<std::pair<int64_t, int64_t>> wsisize;
        GradeMgr *grademgr;
        mmsi::Accessor *segmmgr;

        std::map<std::string, std::string> props;
        std::set<std::string> propnames;

        std::string projdir;
        std::string jsonpath;
        std::string segmpath;
        std::string gradepath;

        Json::Value projobj;

        Json::Value annoarr;
        Json::Value levelarr;
        Json::Value wsiobj;

        int32_t segm_ps = 0;
        int32_t tile_size = 0;
        int32_t grade_size = 0;

        struct {
            int64_t x = 0, y = 0, w = 0, h = 0;
        } rect;
        struct {
            int64_t w = 0, h = 0;
        } size;
        double downsample = 1.0;
        
        int32_t opencv_type;
        bool m_secure_mode = false;
        int32_t comp_level = 3;

        std::string error;
        
        int32_t SelectCorrectLevel(double downsample);
        int32_t SelectCorrectSegmLevel(double downsample);
        
        double GetDownsample(int32_t level);
        void UpdateDownsample();
        
        void SaveProject();
        
    };
    
    class AnnoPrivate : virtual public Anno {
    public:
        
        AnnoPrivate(AnnoType type) : Anno(type) {}
        
        AnnoPrivate(Json::Value &value) : Anno(AnnoType::NONE) {
            Json::Value obj_color = value["color"];
            m_color.r = obj_color["r"].asInt64();
            m_color.g = obj_color["g"].asInt64();
            m_color.b = obj_color["b"].asInt64();
            std::string type = value["type"].asString();
            if (type == "ellipse") m_type = AnnoType::ELLIPSE;
            if (type == "rectangle") m_type = AnnoType::RECTANGLE;
            if (type == "polygon") m_type = AnnoType::POLYGON;
        }
        
        virtual Json::Value ToJson() {
            Json::Value obj_anno, obj_color;
            obj_color["r"] = m_color.r;
            obj_color["g"] = m_color.g;
            obj_color["b"] = m_color.b;
            obj_anno["color"] = obj_color;

            switch (m_type) {
                case AnnoType::ELLIPSE: obj_anno["type"] = "ellipse"; break;
                case AnnoType::RECTANGLE: obj_anno["type"] = "rectangle"; break;
                case AnnoType::POLYGON: obj_anno["type"] = "polygon"; break;
                default: obj_anno["type"] = "none";
            }

            return obj_anno;
        }
        
        static Anno *FromJson(Json::Value &value);
        
    };
    
    class AnnoEllipsePrivate : public AnnoEllipse, AnnoPrivate {
    public:
        
        AnnoEllipsePrivate(int64_t x, int64_t y, int64_t a, int64_t b) : AnnoEllipse(x, y, a, b), AnnoPrivate(AnnoType::ELLIPSE), Anno(AnnoType::ELLIPSE) {}
        
        AnnoEllipsePrivate(Json::Value &value) : AnnoPrivate(value), Anno(AnnoType::ELLIPSE) {
            Json::Value obj_elli = value["value"];
            m_ellipse.x = obj_elli["x"].asInt64();
            m_ellipse.y = obj_elli["y"].asInt64();
            m_ellipse.a = obj_elli["a"].asInt64();
            m_ellipse.b = obj_elli["b"].asInt64();
        }
        
        virtual Json::Value ToJson() override {
            Json::Value obj_elli, obj_anno = AnnoPrivate::ToJson();
            obj_elli["x"] = static_cast<Json::Int64>(m_ellipse.x);
            obj_elli["y"] = static_cast<Json::Int64>(m_ellipse.y);
            obj_elli["a"] = static_cast<Json::Int64>(m_ellipse.a);
            obj_elli["b"] = static_cast<Json::Int64>(m_ellipse.b);
            obj_anno["value"] = obj_elli;
            return obj_anno;
        }
        
    };
    
    class AnnoRectPrivate : public AnnoRect, AnnoPrivate {
    public:
        
        AnnoRectPrivate(int64_t x, int64_t y, int64_t w, int64_t h) : AnnoRect(x, y, w, h), AnnoPrivate(AnnoType::RECTANGLE), Anno(AnnoType::RECTANGLE) {}
        
        AnnoRectPrivate(Json::Value &value) : AnnoPrivate(value), Anno(AnnoType::RECTANGLE) {
            Json::Value obj_rect = value["value"];
            m_rect.x = obj_rect["x"].asInt64();
            m_rect.y = obj_rect["y"].asInt64();
            m_rect.w = obj_rect["w"].asInt64();
            m_rect.h = obj_rect["h"].asInt64();
        }
        
        virtual Json::Value ToJson() override {
            Json::Value obj_rect, obj_anno = AnnoPrivate::ToJson();
            obj_rect["x"] = static_cast<Json::Int64>(m_rect.x);
            obj_rect["y"] = static_cast<Json::Int64>(m_rect.y);
            obj_rect["w"] = static_cast<Json::Int64>(m_rect.w);
            obj_rect["h"] = static_cast<Json::Int64>(m_rect.h);
            obj_anno["value"] = obj_rect;
            return obj_anno;
        }
        
    };
    
    class AnnoPolygonPrivate : public AnnoPolygon, AnnoPrivate {
    public:
        
        AnnoPolygonPrivate(const std::vector<std::pair<int64_t, int64_t>> &points) : AnnoPolygon(points), AnnoPrivate(AnnoType::POLYGON), Anno(AnnoType::POLYGON) {}
        
        AnnoPolygonPrivate(Json::Value &value) : AnnoPrivate(value), Anno(AnnoType::POLYGON) {
            Json::Value obj_poly = value["value"];
            for (auto &point : obj_poly) {
                m_polygon.push_back({ point["x"].asInt64(), point["y"].asInt64() });
            }
        }
    
        virtual Json::Value ToJson() override {
            Json::Value obj_poly, obj_anno = AnnoPrivate::ToJson();
            for (auto &point : m_polygon) {
                Json::Value p;
                p["x"] = static_cast<Json::Int64>(point.first);
                p["y"] = static_cast<Json::Int64>(point.second);
                obj_poly.append(p);
            }
            obj_anno["value"] = obj_poly;
            return obj_anno;
        }
        
    };
    
    class GradeMgr;
    
    class GradeTile {
    public:
        GradeTile(int64_t index, GradeMgr *mgr);
        ~GradeTile();
        
        void ReadData();
        void WriteData();
        
        GradeMgr *m_mgr;
        
        int64_t m_index;
        uint8_t *m_buffer = nullptr;
        
        bool m_modified = false;
        bool m_existed = false;
    };
    
    class GradeMgr {
    public:
        
        GradeMgr(Slide *slide);
        ~GradeMgr();
        
        int8_t GetGrade(int64_t id);
        void SetGrade(int64_t id, int8_t grade);
        
        void GetGrades(int32_t len, const int64_t *ids, int8_t *grades);
        void SetGrades(int32_t len, const int64_t *ids, const int8_t *grades);
        
        Slide *m_slide;
        
        sqlite3 *m_db;
        sqlite3_stmt *m_read_grade, *m_write_grade, *m_insert_grade;
    };

    Slide::Slide(std::string projdir) : m(new Slide::SlidePrivate()) {
        m->projdir = projdir;
        m->jsonpath = projdir + "/project.json";
        m->segmpath = projdir + "/segm.mmsi";
        m->gradepath = projdir + "/grade.db";
    }

    Slide::~Slide() {
        if (m->openslide != nullptr)
            openslide_close(m->openslide);
        for (auto &anno : m->annos)
            delete anno;
        delete m->segmmgr;
        delete m->grademgr;
        delete m;
    }

    void Slide::LoadProject() {
        std::ifstream t(m->jsonpath);
        if (!t.good()) {
            m->error = "Read project json file failed.";
            return;
        }
        
        std::stringstream buffer;
        buffer << t.rdbuf();
        std::string contents(buffer.str());

        /*
        Json::Reader reader;
        if (!reader.parse(contents, m->projobj)) {
            m->error = "Parsing json failed.";
            return;
        }
        */
        
        Json::CharReaderBuilder jsreader;
        std::unique_ptr<Json::CharReader> const reader(jsreader.newCharReader());
        std::string err;
        if (!reader->parse(contents.data(), contents.data() + contents.size(), &m->projobj, &err)) {
            m->error = "Parsing json failed: " + err;
            return;
        }
        
        if (m->projobj["version"].asInt() > OPENHIL_FILE_VERSION) {
            m->error = "This project requires a higher version of OpenHIL.";
            return;
        }

        m->segm_ps = m->projobj["segm_pxw"].asInt();
        m->tile_size = m->projobj["tile_size"].asInt();
        m->grade_size = m->projobj["grade_size"].asInt();
        
        switch (m->segm_ps) {
            case 1: m->opencv_type = CV_8U; break;
            case 2: m->opencv_type = CV_16U; break;
            case 4: m->opencv_type = CV_16UC2; break;
            case 8: m->opencv_type = CV_16UC4; break;
        }
        
        m->levelarr = m->projobj["level"];
        for (auto &size_obj : m->levelarr)
            m->wsisize.push_back({ size_obj["width"].asInt64(), size_obj["height"].asInt64() });
        
        m->segmmgr = new mmsi::Accessor(m->segmpath);
        if (!m->segmmgr->OpenFile()) {
            m->error = m->segmmgr->GetError();
            return;
        }
        
        m->annoarr = m->projobj["anno"];
        for (auto &anno_obj : m->annoarr)
            m->annos.push_back(AnnoPrivate::FromJson(anno_obj));
        
        m->grademgr = new GradeMgr(this);
        
        m->wsiobj = m->projobj["wsi"];
        std::string wsi_path = m->wsiobj["file"].asString();
        if (m->wsiobj["copy"].asBool())
            wsi_path = m->projdir + '/' + wsi_path;

        m->type = SlideType::UNKNOWN;
        std::string stype = m->wsiobj["type"].asString();
        if (stype == "openslide")
            m->type = SlideType::OPENSLIDE;
        else if (stype == "dicom")
            m->type = SlideType::DICOM;
        if (m->type == SlideType::UNKNOWN) {
            m->error = "Unknown wsi format.";
            return;
        }

        Json::Value propobj = m->wsiobj["property"];
        for (auto &pname : propobj.getMemberNames()) {
            m->propnames.insert(pname);
            m->props[pname] = propobj.get(pname, "").asString();
        }

        switch (m->type) {
            case SlideType::OPENSLIDE: {
                m->openslide = openslide_open(wsi_path.data());
                if (m->openslide == nullptr) {
                    m->error = "OpenSlide open file failed.";
                    return;
                }

                auto oserr = openslide_get_error(m->openslide);
                if (oserr != nullptr) {
                    m->error = std::string("OpenSlide open file failed: ") + oserr;
                    openslide_close(m->openslide);
                    m->openslide = nullptr;
                    return;
                }
            }
            break;

            case SlideType::DICOM: {

            }
            break;
            
            default: break;
        }
    }

    void Slide::SaveProject() {
        if (!m->segmmgr->CloseFile())
            m->error = m->segmmgr->GetError();
        m->SaveProject();
    }
    
    void Slide::SlidePrivate::SaveProject() {
        annoarr.clear();
        for (auto &anno : annos)
            annoarr.append(dynamic_cast<AnnoPrivate*>(anno)->ToJson());
        projobj["anno"] = annoarr;
        
        Json::Value propobj;
        for (auto &prop : props)
            propobj[prop.first] = prop.second;
        wsiobj["property"] = propobj;
        projobj["wsi"] = wsiobj;
        
        Json::StreamWriterBuilder jsrocd;
        std::unique_ptr<Json::StreamWriter> write(jsrocd.newStreamWriter());
        std::ostringstream os;
        write->write(projobj, &os);
        std::string strWrite = os.str();
        std::ofstream ofs(jsonpath);
        if (!ofs.good()) {
            error = "Write project json file failed.";
            return;
        }
        
        ofs << strWrite;
        ofs.close();
    }

    bool Slide::HasError() {
        return m->error.length() != 0;
    }

    const std::string &Slide::GetErrorStr() {
        return m->error;
    }

    const std::set<std::string> &Slide::GetPropertyNames() {
        return m->propnames;
    }

    const std::string Slide::GetPropertyValue(const std::string &name) {
        auto iter = m->props.find(name);
        return iter == m->props.end() ? "" : iter->second;
    }

    void Slide::SetPropertyValue(const std::string &name, const std::string &value) {
        m->propnames.insert(name);
        m->props[name] = value;
    }

    void Slide::RemoveProperty(const std::string &name) {
        m->propnames.erase(name);
        m->props.erase(name);
    }

    int64_t Slide::GetWidth() {
        return m->wsisize[0].first;
    }

    int64_t Slide::GetHeight() {
        return m->wsisize[0].second;
    }
    
    SlideType Slide::GetType() {
        return m->type;
    }

    void Slide::SetCurrentRegion(int64_t x, int64_t y, int64_t w, int64_t h) {
        m->rect.x = x, m->rect.y = y, m->rect.w = w, m->rect.h = h;
        m->segmmgr->SetCurrentRegion(x, y, w, h);
    }

    void Slide::SetRegionSize(int64_t w, int64_t h) {
        m->size.w = w, m->size.h = h;
    }

    void Slide::ReadRegion(void *buf) {
        cv::Mat resimg(m->size.h, m->size.w, CV_8UC4, buf);
        m->UpdateDownsample();
        
        switch (m->type) {
            case SlideType::OPENSLIDE: {
                int32_t level = m->SelectCorrectLevel(m->downsample);
                double cur_ds = m->GetDownsample(level);
                int64_t os_read_w = static_cast<int64_t>(m->rect.w * cur_ds);
                int64_t os_read_h = static_cast<int64_t>(m->rect.h * cur_ds);
                cv::Mat osimg(os_read_h, os_read_w, CV_8UC4);
                openslide_read_region(m->openslide, reinterpret_cast<uint32_t*>(osimg.data),
                        m->rect.x, m->rect.y, level, os_read_w, os_read_h);
                cv::resize(osimg, resimg, cv::Size(m->size.w, m->size.h));
            }
            break;
            case SlideType::DICOM: {

            }
            break;
            default: break;
        }
    }
    
    void Slide::ReadSegm(void *buf) {
        cv::Mat rmat(m->size.h, m->size.w, m->opencv_type, buf);
        m->UpdateDownsample();
        
        int32_t level = m->SelectCorrectSegmLevel(m->downsample);
        int64_t iw = m->segmmgr->GetRegionWidthLevel(level);
        int64_t ih = m->segmmgr->GetRegionHeightLevel(level);
        
        cv::Mat omat(ih, iw, m->opencv_type);
        m->segmmgr->ReadRegion(level, omat.data);
        if (m->segmmgr->HasError())
            m->error = m->segmmgr->GetError();
        cv::resize(omat, rmat, cv::Size(m->size.w, m->size.h), 0, 0, cv::INTER_NEAREST);
    }
    
    void Slide::WriteSegm(void *buf, void *mask) {
        m->UpdateDownsample();
        int32_t level = m->SelectCorrectSegmLevel(m->downsample);
        int64_t iw = m->segmmgr->GetRegionWidthLevel(level);
        int64_t ih = m->segmmgr->GetRegionHeightLevel(level);
        
        cv::Mat omat(m->size.h, m->size.w, m->opencv_type, buf);
        cv::Mat rmat(ih, iw, m->opencv_type);
        cv::resize(omat, rmat, cv::Size(iw, ih), 0, 0, cv::INTER_NEAREST);
        
        if (mask != nullptr) {
            cv::Mat omask(m->size.h, m->size.w, CV_8U, mask);
            cv::Mat rmask(ih, iw, CV_8U);
            cv::resize(omask, rmask, cv::Size(iw, ih), 0, 0, cv::INTER_NEAREST);
            m->segmmgr->WriteRegion(level, rmat.data, rmask.data);
        } else {
            m->segmmgr->WriteRegion(level, rmat.data, nullptr);
        }
        
        if (m->segmmgr->HasError())
            m->error = m->segmmgr->GetError();
    }
    
    int32_t Slide::SlidePrivate::SelectCorrectLevel(double downsample) {
        double targ = downsample;
        for (int i = 1; i < wsisize.size(); i++) {
            auto &size = wsisize[i];
            double cur = GetDownsample(i);
            if (targ < cur)
                return i - 1;
        }
        return wsisize.size() - 1;
    }
    
    int32_t Slide::SlidePrivate::SelectCorrectSegmLevel(double downsample) {
        for (int i = 1; i < segmmgr->GetLevelCount(); i++)
            if (downsample < 1 << i)
                return i - 1;
        return segmmgr->GetLevelCount() - 1;
    }
    
    double Slide::SlidePrivate::GetDownsample(int32_t level) {
        return static_cast<double>(wsisize[level].first) / wsisize[0].first;
    }
    
    void Slide::SlidePrivate::UpdateDownsample() {
        downsample = std::min(static_cast<double>(rect.w) / size.w,
                static_cast<double>(rect.h) / size.h);
    }

    AnnoEllipse* Slide::AddAnnoEllipse(int64_t x, int64_t y, int64_t a, int64_t b) {
        AnnoEllipsePrivate *ret = new AnnoEllipsePrivate(x, y, a, b);
        m->annos.push_back(ret);
        return ret;
    }

    AnnoRect* Slide::AddAnnoRect(int64_t x, int64_t y, int64_t w, int64_t h) {
        AnnoRectPrivate *ret = new AnnoRectPrivate(x, y, w, h);
        m->annos.push_back(ret);
        return ret;
    }

    AnnoPolygon* Slide::AddAnnoPolygon(const std::vector<std::pair<int64_t, int64_t>> &points) {
        AnnoPolygonPrivate *ret = new AnnoPolygonPrivate(points);
        m->annos.push_back(ret);
        return ret;
    }
    
    const std::vector<Anno*> &Slide::GetAnnos() {
        return m->annos;
    }

    void Slide::RemoveAnno(Anno *anno) {
        auto iter = std::find(m->annos.begin(), m->annos.end(), anno);
        if (iter != m->annos.end()) {
            m->annos.erase(iter);
            delete anno;
        }
    }
    
    void Slide::SetSegmCacheSize(int32_t size) {
        m->segmmgr->SetCacheSize(size);
    }
    
    void Slide::SetSecureMode(bool enable) {
        m->segmmgr->SetSecureMode(enable);
        m->m_secure_mode = enable;
    }
    
    int32_t Slide::GetSegmTileSize() {
        return m->tile_size;
    }
    
    int32_t Slide::GetSegmPixelSize() {
        return m->segm_ps;
    }
    
    int32_t Slide::GetGradeTileSize() {
        return m->grade_size;
    }
    
    void Slide::SetCompressLevel(int32_t level) {
        m->segmmgr->SetCompressLevel(level);
    }
    
    int8_t Slide::GetGrade(int64_t id) {
        return m->grademgr->GetGrade(id);
    }

    void Slide::SetGrade(int64_t id, int8_t grade) {
        m->grademgr->SetGrade(id, grade);
    }
    
    void Slide::GetGrades(int32_t len, const int64_t *ids, int8_t *grades) {
        m->grademgr->GetGrades(len, ids, grades);
    }
    
    void Slide::SetGrades(int32_t len, const int64_t *ids, const int8_t *grades) {
        m->grademgr->SetGrades(len, ids, grades);
    }

    Anno::Anno(AnnoType type) : m_type(type) {
    }

    Anno::~Anno() {
    }

    AnnoType Anno::getType() {
        return m_type;
    }
    
    AnnoEllipse::AnnoEllipse(int64_t x, int64_t y, int64_t a, int64_t b) : Anno(AnnoType::ELLIPSE) {
        m_ellipse.x = x, m_ellipse.y = y, m_ellipse.a = a, m_ellipse.b = b;
    }
    
    AnnoEllipse::~AnnoEllipse() {
    }
    
    AnnoRect::AnnoRect(int64_t x, int64_t y, int64_t w, int64_t h) : Anno(AnnoType::RECTANGLE) {
        m_rect.x = x, m_rect.y = y, m_rect.w = w, m_rect.h = h;
    }
    
    AnnoRect::~AnnoRect() {
    }
    
    AnnoPolygon::AnnoPolygon(const std::vector<std::pair<int64_t,int64_t>>& points) : Anno(AnnoType::POLYGON) {
        m_polygon = points;
    }
    
    AnnoPolygon::~AnnoPolygon() {
    }
    
    Anno *AnnoPrivate::FromJson(Json::Value& value)  {
        std::string type = value["type"].asString();
        if (type == "ellipse") return new AnnoEllipsePrivate(value);
        if (type == "rectangle") return new AnnoRectPrivate(value);
        if (type == "polygon") return new AnnoPolygonPrivate(value);
        return nullptr;
    }
    
    GradeMgr::GradeMgr(Slide *slide) {
        m_slide = slide;
        
        sqlite3_open_v2(slide->m->gradepath.data(), &m_db, SQLITE_OPEN_READWRITE | 
            SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_SHAREDCACHE, nullptr);
#ifdef SQLITE_PREPARE_PERSISTENT
        sqlite3_prepare_v3(m_db, sql_read_grade, sizeof(sql_read_grade) + 1,
            SQLITE_PREPARE_PERSISTENT, &m_read_grade, nullptr);
        sqlite3_prepare_v3(m_db, sql_write_grade, sizeof(sql_write_grade) + 1,
            SQLITE_PREPARE_PERSISTENT, &m_write_grade, nullptr);
        sqlite3_prepare_v3(m_db, sql_insert_grade, sizeof(sql_insert_grade) + 1,
            SQLITE_PREPARE_PERSISTENT, &m_insert_grade, nullptr);
#else
        sqlite3_prepare_v2(m_db, sql_read_grade, sizeof(sql_read_grade) + 1,
            &m_read_grade, nullptr);
        sqlite3_prepare_v2(m_db, sql_write_grade, sizeof(sql_write_grade) + 1,
            &m_write_grade, nullptr);
        sqlite3_prepare_v2(m_db, sql_insert_grade, sizeof(sql_insert_grade) + 1,
            &m_insert_grade, nullptr);
#endif
    }
    
    GradeMgr::~GradeMgr() {
        sqlite3_finalize(m_read_grade);
        sqlite3_finalize(m_write_grade);
        sqlite3_finalize(m_insert_grade);
        
        sqlite3_close(m_db);
    }

    int8_t GradeMgr::GetGrade(int64_t id) {
        int64_t grade_size = m_slide->GetGradeTileSize();
        int64_t index = id / grade_size;
        id %= grade_size;
        
        GradeTile *tile = new GradeTile(index, this);
        int8_t ret = tile->m_buffer[id];
        delete tile;
        return ret;
    }
    
    void GradeMgr::SetGrade(int64_t id, int8_t grade) {
        int64_t grade_size = m_slide->GetGradeTileSize();
        int64_t index = id / grade_size;
        id %= grade_size;
        
        GradeTile *tile = new GradeTile(index, this);
        tile->m_buffer[id] = grade;
        tile->m_modified = true;
        delete tile;
    }
        
    void GradeMgr::GetGrades(int32_t len, const int64_t *ids, int8_t *grades) {
        std::vector<std::pair<int64_t, int8_t*>> ops(len);
        for (int32_t i = 0; i < len; i++)
            ops[i] = { ids[i], &grades[i] };
        std::sort(ops.begin(), ops.end());
        int64_t grade_size = m_slide->GetGradeTileSize();
        
        for (int32_t s = 0; s < ops.size(); s++) {
            int64_t index = ops[s].first / grade_size;
            int32_t e = s + 1;
            for ( ; e < ops.size(); e++)
                if (ops[e].first / grade_size != index)
                    break;
            
            GradeTile *tile = new GradeTile(index, this);
            for (int32_t i = s; i < e; i++)
                *ops[i].second = tile->m_buffer[ops[i].first % grade_size];
            delete tile;
        }
    }
    
    void GradeMgr::SetGrades(int32_t len, const int64_t *ids, const int8_t *grades) {
        std::vector<std::pair<int64_t, int8_t>> ops(len);
        for (int32_t i = 0; i < len; i++)
            ops[i] = { ids[i], grades[i] };
        std::sort(ops.begin(), ops.end());
        int64_t grade_size = m_slide->GetGradeTileSize();
        
        for (int32_t s = 0; s < ops.size(); s++) {
            int64_t index = ops[s].first / grade_size;
            int32_t e = s + 1;
            for ( ; e < ops.size(); e++)
                if (ops[e].first / grade_size != index)
                    break;
            
            GradeTile *tile = new GradeTile(index, this);
            for (int32_t i = s; i < e; i++)
                tile->m_buffer[ops[i].first % grade_size] = ops[i].second;
            tile->m_modified = true;
            delete tile;
        }
    }
    
    GradeTile::GradeTile(int64_t index, GradeMgr *mgr) {
        m_mgr = mgr;
        
        m_index = index;
        ReadData();
    }
    
    GradeTile::~GradeTile() {
        WriteData();
    }
    
    void GradeTile::ReadData() {
        if (m_buffer == nullptr) {
            m_modified = false;
            uint64_t osize = m_mgr->m_slide->GetGradeTileSize();
            m_buffer = new uint8_t[osize];
            sqlite3_reset(m_mgr->m_read_grade);
            sqlite3_bind_int(m_mgr->m_read_grade, 1, m_index);
            if (SQLITE_ROW == sqlite3_step(m_mgr->m_read_grade)) {
                m_existed = true;
                uint64_t csize = sqlite3_column_bytes(m_mgr->m_read_grade, 0);
                if (csize > 0)
                    uncompress(m_buffer, reinterpret_cast<unsigned long*>(&osize), static_cast<const uint8_t*>(sqlite3_column_blob(m_mgr->m_read_grade, 0)), csize);
                else memset(m_buffer, 0, osize);
            } else memset(m_buffer, 0, osize);
        }
    }
    
    void GradeTile::WriteData() {
        if (m_buffer != nullptr) {
            if (m_modified) {
                uint64_t osize = m_mgr->m_slide->GetGradeTileSize();
                uint64_t csize = compressBound(osize);
                uint8_t *filebuf = static_cast<uint8_t*>(std::malloc(csize));
                compress2(filebuf, reinterpret_cast<unsigned long*>(&csize), m_buffer, osize, m_mgr->m_slide->m->comp_level);
                
                sqlite3_stmt *stmt = m_existed ? m_mgr->m_write_grade : m_mgr->m_insert_grade;
                sqlite3_reset(stmt);
                sqlite3_bind_int(stmt, 1, m_index);
                sqlite3_bind_blob(stmt, 2, filebuf, csize, std::free);
                sqlite3_step(stmt);
            }
            
            delete m_buffer;
            m_buffer = nullptr;
        }
    }
    
    struct OHILSlide {
        Slide *slide;
        
        std::string prop;
        std::set<std::string> props;
        OHILAnno **c_annos = nullptr;
        char **c_props = nullptr;
        
        OHILSlide(Slide *s) {
            slide = s;
        }
        
        ~OHILSlide() {
            if (c_annos) delete[] c_annos;
            if (c_props) delete[] c_props;
            delete slide;
        }
    };
    
    const char *ohil_Version() {
        return OPENHIL_VERSION;
    }

    OHILSlide *ohil_Create(const char *projdir) {
        return new OHILSlide(new Slide(projdir));
    }

    void ohil_Destroy(OHILSlide *slide) {
        delete slide;
    }

    void ohil_LoadProject(OHILSlide *slide) {
        slide->slide->LoadProject();
    }

    void ohil_SaveProject(OHILSlide *slide) {
        slide->slide->SaveProject();
    }

    int64_t ohil_GetWidth(OHILSlide *slide) {
        return slide->slide->GetWidth();
    }

    int64_t ohil_GetHeight(OHILSlide *slide) {
        return slide->slide->GetHeight();
    }

    SlideType ohil_GetType(OHILSlide *slide) {
        return slide->slide->GetType();
    }

    void ohil_SetCurrentRegion(OHILSlide *slide, int64_t x, int64_t y, int64_t w, int64_t h) {
        slide->slide->SetCurrentRegion(x, y, w, h);
    }

    void ohil_SetRegionSize(OHILSlide *slide, int64_t w, int64_t h) {
        slide->slide->SetRegionSize(w, h);
    }

    void ohil_ReadRegion(OHILSlide *slide, void *buf) {
        slide->slide->ReadRegion(buf);
    }

    void ohil_ReadSegm(OHILSlide *slide, void *buf) {
        slide->slide->ReadSegm(buf);
    }

    void ohil_WriteSegm(OHILSlide *slide, void *buf, void *mask) {
        slide->slide->WriteSegm(buf, mask);
    }

    OHILAnno* ohil_AddAnnoEllipse(OHILSlide *slide, int64_t x, int64_t y, int64_t a, int64_t b) {
        return slide->slide->AddAnnoEllipse(x, y, a, b);
    }

    OHILAnno* ohil_AddAnnoRect(OHILSlide *slide, int64_t x, int64_t y, int64_t w, int64_t h) {
        return slide->slide->AddAnnoRect(x, y, w, h);
    }

    OHILAnno* ohil_AddAnnoPolygon(OHILSlide *slide, const int64_t *x, const int64_t *y, int32_t count) {
        std::vector<std::pair<int64_t, int64_t>> vec(count);
        for (int32_t i = 0; i < count; i++)
            vec[i] = { x[i], y[i] };
        return slide->slide->AddAnnoPolygon(vec);
    }

    const OHILAnno *const *ohil_GetAnnos(OHILSlide *slide) {
        if (slide->c_annos) delete[] slide->c_annos;
        int32_t cnt = slide->slide->GetAnnos().size();
        slide->c_annos = new OHILAnno*[cnt + 1];
        int32_t i = 0;
        for ( ; i < cnt; i++)
            slide->c_annos[i] = slide->slide->GetAnnos()[i];
        slide->c_annos[i] = nullptr;
        return slide->c_annos;
    }

    void ohil_RemoveAnno(OHILSlide *slide, OHILAnno *anno) {
        slide->slide->RemoveAnno(anno);
    }

    void ohil_SetSegmCacheSize(OHILSlide *slide, int32_t size) {
        slide->slide->SetSegmCacheSize(size);
    }

    void ohil_SetSecureMode(OHILSlide *slide, bool enable) {
        slide->slide->SetSecureMode(enable);
    }
    
     void ohil_SetCompressLevel(OHILSlide *slide, int32_t level) {
         slide->slide->SetCompressLevel(level);
     }

    const char *const *ohil_GetPropertyNames(OHILSlide *slide) {
        if (slide->c_props) delete[] slide->c_props;
        slide->props = slide->slide->GetPropertyNames();
        slide->c_props = new char*[slide->props.size() + 1];
        int32_t i = 0;
        for (auto &name : slide->props)
            slide->c_props[i++] = const_cast<char*>(name.data());
        slide->c_props[i] = nullptr;
        return slide->c_props;
    }

    const char *ohil_GetPropertyValue(OHILSlide *slide, const char *name) {
        slide->prop = slide->slide->GetPropertyValue(name);
        return slide->prop.data();
    }

    void ohil_SetPropertyValue(OHILSlide *slide, const char *name, const char *value) {
        slide->slide->SetPropertyValue(name, value);
    }

    void ohil_RemoveProperty(OHILSlide *slide, const char *name) {
        slide->slide->RemoveProperty(name);
    }

    int8_t ohil_GetGrade(OHILSlide *slide, int64_t id) {
        return slide->slide->GetGrade(id);
    }

    void ohil_GetGrades(OHILSlide *slide, int32_t len, const int64_t *ids, int8_t *grades) {
        slide->slide->GetGrades(len, ids, grades);
    }

    void ohil_SetGrade(OHILSlide *slide, int64_t id, int8_t grade) {
        slide->slide->SetGrade(id, grade);
    }

    void ohil_SetGrades(OHILSlide *slide, int32_t len, const int64_t *ids, const int8_t *grades) {
        slide->slide->SetGrades(len, ids, grades);
    }

    bool ohil_HasError(OHILSlide *slide) {
        return slide->slide->HasError();
    }

    const char *ohil_GetErrorStr(OHILSlide *slide) {
        return slide->slide->GetErrorStr().data();
    }

    int32_t ohil_GetSegmTileSize(OHILSlide *slide) {
        return slide->slide->GetSegmTileSize();
    }

    int32_t ohil_GetSegmPixelSize(OHILSlide *slide) {
        return slide->slide->GetSegmPixelSize();
    }

    int32_t ohil_GetGradeTileSize(OHILSlide *slide) {
        return slide->slide->GetGradeTileSize();
    }

    AnnoType ohil_GetAnnoType(OHILAnno *anno) {
        return anno->getType();
    }

    void ohil_GetAnnoColor(OHILAnno *anno, uint8_t *r, uint8_t *g, uint8_t *b) {
        *r = anno->m_color.r;
        *g = anno->m_color.g;
        *b = anno->m_color.b;
    }

    void ohil_SetAnnoColor(OHILAnno *anno, uint8_t r, uint8_t g, uint8_t b) {
        anno->m_color.r = r;
        anno->m_color.g = g;
        anno->m_color.b = b;
    }

    void ohil_GetAnnoRectParam(OHILAnno *anno, int64_t *x, int64_t *y, int64_t *w, int64_t *h) {
        AnnoRect *rect = dynamic_cast<AnnoRect*>(anno);
        *x = rect->m_rect.x;
        *y = rect->m_rect.y;
        *w = rect->m_rect.w;
        *h = rect->m_rect.h;
    }

    void ohil_SetAnnoRectParam(OHILAnno *anno, int64_t x, int64_t y, int64_t w, int64_t h) {
        AnnoRect *rect = dynamic_cast<AnnoRect*>(anno);
        rect->m_rect.x = x;
        rect->m_rect.y = y;
        rect->m_rect.w = w;
        rect->m_rect.h = h;
    }

    void ohil_GetAnnoElliParam(OHILAnno *anno, int64_t *x, int64_t *y, int64_t *a, int64_t *b) {
        AnnoEllipse *elli = dynamic_cast<AnnoEllipse*>(anno);
        *x = elli->m_ellipse.x;
        *y = elli->m_ellipse.y;
        *a = elli->m_ellipse.a;
        *b = elli->m_ellipse.b;
    }

    void ohil_SetAnnoElliParam(OHILAnno *anno, int64_t x, int64_t y, int64_t a, int64_t b) {
        AnnoEllipse *elli = dynamic_cast<AnnoEllipse*>(anno);
        elli->m_ellipse.x = x;
        elli->m_ellipse.y = y;
        elli->m_ellipse.a = a;
        elli->m_ellipse.b = b;
    }

    int32_t ohil_GetAnnoPolyPointCount(OHILAnno *anno) {
        return dynamic_cast<AnnoPolygon*>(anno)->m_polygon.size();
    }

    void ohil_GetAnnoPolyParam(OHILAnno *anno, int64_t *x, int64_t *y) {
        AnnoPolygon *poly = dynamic_cast<AnnoPolygon*>(anno);
        for (int32_t i = 0; i < poly->m_polygon.size(); i++) {
            x[i] = poly->m_polygon[i].first;
            y[i] = poly->m_polygon[i].second;
        }
    }

    void ohil_SetAnnoPolyParam(OHILAnno *anno, int64_t *x, int64_t *y, int32_t count) {
        AnnoPolygon *poly = dynamic_cast<AnnoPolygon*>(anno);
        poly->m_polygon.resize(count);
        for (int32_t i = 0; i < poly->m_polygon.size(); i++)
            poly->m_polygon[i] = { x[i], y[i] };
    }

}