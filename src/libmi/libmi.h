/*
 * Copyright (C) 2018 GPIAY
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   libmi.h
 * Author: GPIAY
 *
 * Created on November 5, 2018, 6:50 PM
 */

#ifndef OPENHIL_H
#define OPENHIL_H

#ifdef WIN32

#ifdef LIBMI_EXPORTS
#define OHIL_EXPORT __declspec(dllexport)
#else
#define OHIL_EXPORT __declspec(dllimport)
#endif

#else

#define OHIL_EXPORT __attribute__ ((visibility("default")))

#endif

#ifdef __cplusplus
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#else
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#ifdef __cplusplus
namespace ohil {
#endif
    
/**
 * The type of the whole-slide image. Currently include OpenSlide supported
 * formats and the DICOM standard.
 */
enum OHIL_EXPORT SlideType {
    OPENSLIDE,
    DICOM,
    UNKNOWN,
};

/**
 * The type of annotations.
 */
enum OHIL_EXPORT AnnoType {
    NONE,
    ELLIPSE,
    RECTANGLE,
    POLYGON,
};

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

namespace ohil {

class Anno;
class AnnoEllipse;
class AnnoRect;
class AnnoPolygon;
class GradeTile;
class GradeMgr;

/**
 * The main class of OpenHIL. Each object of HSlide represents an OpenHIL
 * project. All operations will go through this class.
 */
class OHIL_EXPORT Slide {
    friend class GradeTile;
    friend class GradeMgr;
public:

    /**
     * Constructor of HSlide class.
     * 
     * @param projdir The directory path of the OpenHIL project.
     */
    Slide(const std::string projdir);
    virtual ~Slide();

    /**
     * Load the OpenHIL project.
     * 
     * Do not perform any other actions before calling this function.
     */
    void LoadProject();

    /**
     * Save the OpenHIL project.
     * 
     * This function must be called at last or some data will lost.
     */
    void SaveProject();

    /**
     * Get the width of the whole-slide image.
     * 
     * @return The width of the image.
     */
    int64_t GetWidth();

    /**
     * Get the height of the whole-slide image.
     * 
     * @return The height of the image.
     */
    int64_t GetHeight();
    
    /**
     * Get the type of this whole-slide image.
     * 
     * @return The type.
     */
    SlideType GetType();

    /**
     * Set the current operating region.
     * 
     * @param x The x coordination of the top-left corner.
     * @param y The y coordination of the top-left corner.
     * @param w The width of the region.
     * @param h The height of the region.
     */
    void SetCurrentRegion(int64_t x, int64_t y, int64_t w, int64_t h);

    /**
     * Set the resolution when reading or writing regions.
     * 
     * @param w The width of the region.
     * @param h The height of the region.
     */
    void SetRegionSize(int64_t w, int64_t h);

    /**
     * Read pixel data of the operating region of the image.
     * 
     * This function returns ARGB data so parameter buf must be a valid
     * pointer with enough space to hold the data. The resolution is based
     * on the region size, so buf has to be at least (w * h * 4) bytes.
     * The reference coordination is based on the operating region.
     * 
     * @param buf The memory to hold the data.
     */
    void ReadRegion(void *buf);

    /**
     * Read segmentation data of the operating region from the image.
     * 
     * This function returns an array of segmentation id so parameter buf
     * must be a valid pointer with at least (w * h * segmentation
     * pixel size) bytes space to hold the data. The reference coordination
     * is based on the operating region.
     * 
     * @param buf The memory that holds the segmentation data.
     */
    void ReadSegm(void *buf);

    /**
     * Write segmentation data of the operating region to the image.
     * 
     * This function requires an array of segmentation id so parameter buf
     * must be a valid pointer with at least (w * h * segmentation
     * pixel size) bytes space to hold the data and mask must be at least
     * (w * h) bytes. The reference coordination is based on the operating
     * region. Mask can be null. A non-zero byte in mask means to write the
     * data, and a zero byte means to ignore the data.
     * 
     * @param buf The memory that holds the segmentation data.
     * @param mask The memory that holds the mask data.
     */
    void WriteSegm(void *buf, void *mask);

    /**
     * Add a new ellipse annotation to the image.
     * 
     * @param x The x coordination of center.
     * @param y The y coordination of center.
     * @param a The length of x-axis radius.
     * @param b The length of y-axis radius.
     * @return The annotation object.
     */
    AnnoEllipse* AddAnnoEllipse(int64_t x, int64_t y, int64_t a, int64_t b);

    /**
     * Add a new rectangle annotation to the image.
     * 
     * @param x The x coordination of the top-left corner.
     * @param y The y coordination of the top-left corner.
     * @param w The width of the rectangle.
     * @param h The height of the rectangle.
     * @return The annotation object.
     */
    AnnoRect* AddAnnoRect(int64_t x, int64_t y, int64_t w, int64_t h);

    /**
     * Add a new polygon annotation to the image.
     * 
     * @param points The coordinations of points constructing the polygon.
     * @return The annotation object.
     */
    AnnoPolygon* AddAnnoPolygon(const std::vector<std::pair<int64_t, int64_t>> &points);

    /**
     * Get the list of annotations.
     * 
     * This function is only used to read annotations. Do not add or remove
     * annotations through this function.
     * 
     * @return The annotation vector object.
     */
    const std::vector<Anno*> &GetAnnos();

    /**
     * Remove the annotation from the image.
     * 
     * @param anno The annotation to remove.
     */
    void RemoveAnno(Anno *anno);

    /**
     * Set segmentation processing cache size. How many tiles are kept in
     * the memory after each operation.
     * 
     * @param size The cache size.
     */
    void SetSegmCacheSize(int32_t size);

    /**
     * Secure mode ensures that information will not be corrupted once the
     * program crushes. When it is activated the cache system is disabled so
     * it will lower the performance.
     * 
     * @param enable Enable secure mode or not.
     */
    void SetSecureMode(bool enable);

    /**
     * Get all property names in the project.
     * 
     * @return property names set.
     */
    const std::set<std::string> &GetPropertyNames();

    /**
     * Get the value of certain property in the project.
     * 
     * @param name The name of the property.
     * @return The value of the property.
     */
    const std::string GetPropertyValue(const std::string &name);

    /**
     * Set the value of certain property in the project.
     * 
     * @param name The name of the property.
     * @param value The new value for the property.
     */
    void SetPropertyValue(const std::string &name, const std::string &value);

    /**
     * Remove certain property from the project.
     * 
     * @param name The name of the property.
     */
    void RemoveProperty(const std::string &name);

    /**
     * Get grading information for certain segmentation id.
     * 
     * @param id The id of segmentation.
     * @return The grade.
     */
    int8_t GetGrade(int64_t id);

    /**
     * Get bulk grading information.
     * 
     * @param len The length of both ids and grades;
     * @param ids The array containing segmentation id.
     * @param grades The array containing grade.
     */
    void GetGrades(int32_t len, const int64_t *ids, int8_t *grades);

    /**
     * Set grading information for certain segmentation id.
     * 
     * @param id The id of segmentation.
     * @param grade The grade. Use grade = 0 to clear the grade.
     */
    void SetGrade(int64_t id, int8_t grade);

    /**
     * Set bulk grading information.
     * 
     * @param len The length of both ids and grades;
     * @param ids The array containing segmentation id.
     * @param grades The array containing grade.
     */
    void SetGrades(int32_t len, const int64_t *ids, const int8_t *grades);

    /**
     * Is there any error message.
     * 
     * @return Whether an error has occured or not.
     */
    bool HasError();

    /**
     * Get the error message string.
     * 
     * @return The error message.
     */
    const std::string &GetErrorStr();

    /**
     * Get the segmentation tile size.
     * 
     * @return Tile size in pixels.
     */
    int32_t GetSegmTileSize();

    /**
     * Get the segmentation id size (1, 2, 4, 8 bytes).
     * 
     * @return Id size in bytes.
     */
    int32_t GetSegmPixelSize();

    /**
     * Get the grade tile size in bytes;
     * 
     * @return Tile size in bytes;
     */
    int32_t GetGradeTileSize();
    
    /**
     * Set the data compression level (1 - 9). Set 1 for the minimal compression
     * time and set 9 for the minimal file size.
     * 
     * @param level The compression level;
     */
    void SetCompressLevel(int32_t level);

private:

    class SlidePrivate;
    SlidePrivate *m;

};

/**
 * The base class of all annotation classes. Be used to make other annotations.
 */
class OHIL_EXPORT Anno {
    friend class Slide;
public:

    virtual ~Anno();

    /**
     * Get the type of this annotation.
     * 
     * @return The type.
     */
    AnnoType getType();

    struct {
        uint8_t r = 255, g = 255, b = 255;
    } m_color;

protected:

    AnnoType m_type;

    Anno(AnnoType type);

};

class OHIL_EXPORT AnnoEllipse : virtual public Anno {
    friend class Anno;
    friend class Slide;
public:

    virtual ~AnnoEllipse();

    /**
     * The parameters of the ellipse.
     */
    struct {
        int64_t x = 0, y = 0, a = 0, b = 0;
    } m_ellipse;

protected:

    AnnoEllipse(int64_t x, int64_t y, int64_t a, int64_t b);
    AnnoEllipse() : Anno(AnnoType::ELLIPSE) {}

};

class OHIL_EXPORT AnnoRect : virtual public Anno {
    friend class Anno;
    friend class Slide;
public:

    virtual ~AnnoRect();

    /**
     * The parameters of the rectangle.
     */
    struct {
        int64_t x = 0, y = 0, w = 0, h = 0;
    } m_rect;

protected:

    AnnoRect(int64_t x, int64_t y, int64_t w, int64_t h);
    AnnoRect() : Anno(AnnoType::RECTANGLE) {}

};

class OHIL_EXPORT AnnoPolygon : virtual public Anno {
    friend class Anno;
    friend class Slide;
public:

    virtual ~AnnoPolygon();

    /**
     * The points of the polygon.
     */
    std::vector<std::pair<int64_t, int64_t>> m_polygon;

protected:

    AnnoPolygon(const std::vector<std::pair<int64_t, int64_t>> &points);
    AnnoPolygon() : Anno(AnnoType::POLYGON) {}

};
    
extern "C" {
#endif

struct OHILSlide;
#ifdef __cplusplus
typedef Anno OHILAnno;
#else
struct OHILAnno;
#endif

/**
 * Get the openhil library version string.
 * 
 * @return Version.
 */
OHIL_EXPORT const char *ohil_Version();

OHIL_EXPORT OHILSlide *ohil_Create(const char *projdir);
OHIL_EXPORT void ohil_Destroy(OHILSlide *slide);

OHIL_EXPORT void ohil_LoadProject(OHILSlide *slide);
OHIL_EXPORT void ohil_SaveProject(OHILSlide *slide);

OHIL_EXPORT int64_t ohil_GetWidth(OHILSlide *slide);
OHIL_EXPORT int64_t ohil_GetHeight(OHILSlide *slide);
OHIL_EXPORT SlideType ohil_GetType(OHILSlide *slide);

OHIL_EXPORT void ohil_SetCurrentRegion(OHILSlide *slide, int64_t x, int64_t y, int64_t w, int64_t h);
OHIL_EXPORT void ohil_SetRegionSize(OHILSlide *slide, int64_t w, int64_t h);
OHIL_EXPORT void ohil_ReadRegion(OHILSlide *slide, void *buf);
OHIL_EXPORT void ohil_ReadSegm(OHILSlide *slide, void *buf);
OHIL_EXPORT void ohil_WriteSegm(OHILSlide *slide, void *buf, void *mask);

OHIL_EXPORT OHILAnno* ohil_AddAnnoEllipse(OHILSlide *slide, int64_t x, int64_t y, int64_t a, int64_t b);
OHIL_EXPORT OHILAnno* ohil_AddAnnoRect(OHILSlide *slide, int64_t x, int64_t y, int64_t w, int64_t h);
OHIL_EXPORT OHILAnno* ohil_AddAnnoPolygon(OHILSlide *slide, const int64_t *x, const int64_t *y, int32_t count);
OHIL_EXPORT const OHILAnno *const *ohil_GetAnnos(OHILSlide *slide);
OHIL_EXPORT void ohil_RemoveAnno(OHILSlide *slide, OHILAnno *anno);

OHIL_EXPORT void ohil_SetSegmCacheSize(OHILSlide *slide, int32_t size);
OHIL_EXPORT void ohil_SetSecureMode(OHILSlide *slide, bool enable);
OHIL_EXPORT void ohil_SetCompressLevel(OHILSlide *slide, int32_t level);

OHIL_EXPORT const char *const *ohil_GetPropertyNames(OHILSlide *slide);
OHIL_EXPORT const char *ohil_GetPropertyValue(OHILSlide *slide, const char *name);
OHIL_EXPORT void ohil_SetPropertyValue(OHILSlide *slide, const char *name, const char *value);
OHIL_EXPORT void ohil_RemoveProperty(OHILSlide *slide, const char *name);

OHIL_EXPORT int8_t ohil_GetGrade(OHILSlide *slide, int64_t id);
OHIL_EXPORT void ohil_GetGrades(OHILSlide *slide, int32_t len, const int64_t *ids, int8_t *grades);
OHIL_EXPORT void ohil_SetGrade(OHILSlide *slide, int64_t id, int8_t grade);
OHIL_EXPORT void ohil_SetGrades(OHILSlide *slide, int32_t len, const int64_t *ids, const int8_t *grades);

OHIL_EXPORT bool ohil_HasError(OHILSlide *slide);
OHIL_EXPORT const char *ohil_GetErrorStr(OHILSlide *slide);

OHIL_EXPORT int32_t ohil_GetSegmTileSize(OHILSlide *slide);
OHIL_EXPORT int32_t ohil_GetSegmPixelSize(OHILSlide *slide);
OHIL_EXPORT int32_t ohil_GetGradeTileSize(OHILSlide *slide);

OHIL_EXPORT AnnoType ohil_GetAnnoType(OHILAnno *anno);
OHIL_EXPORT void ohil_GetAnnoColor(OHILAnno *anno, uint8_t *r, uint8_t *g, uint8_t *b);
OHIL_EXPORT void ohil_SetAnnoColor(OHILAnno *anno, uint8_t r, uint8_t g, uint8_t b);

OHIL_EXPORT void ohil_GetAnnoRectParam(OHILAnno *anno, int64_t *x, int64_t *y, int64_t *w, int64_t *h);
OHIL_EXPORT void ohil_SetAnnoRectParam(OHILAnno *anno, int64_t x, int64_t y, int64_t w, int64_t h);
OHIL_EXPORT void ohil_GetAnnoElliParam(OHILAnno *anno, int64_t *x, int64_t *y, int64_t *a, int64_t *b);
OHIL_EXPORT void ohil_SetAnnoElliParam(OHILAnno *anno, int64_t x, int64_t y, int64_t a, int64_t b);
OHIL_EXPORT int32_t ohil_GetAnnoPolyPointCount(OHILAnno *anno);
OHIL_EXPORT void ohil_GetAnnoPolyParam(OHILAnno *anno, int64_t *x, int64_t *y);
OHIL_EXPORT void ohil_SetAnnoPolyParam(OHILAnno *anno, int64_t *x, int64_t *y, int32_t count);

#ifdef __cplusplus
}
}
#endif

#endif /* OPENHIL_H */

