CREATE TABLE mmsi_props (
	prop_id INTEGER PRIMARY KEY AUTOINCREMENT,
	prop_key VARCHAR(255) NOT NULL,
	prop_value LONGTEXT NOT NULL DEFAULT ''
	);
CREATE TABLE mmsi_layers (
	layer_id INTEGER PRIMARY KEY AUTOINCREMENT,
	layer_width BIGINT NOT NULL,
	layer_height BIGINT NOT NULL,
	tile_start_id BIGINT NOT NULL,
	tile_x_count BIGINT NOT NULL,
	tile_y_count BIGINT NOT NULL
	);
CREATE TABLE mmsi_tiles (
	tile_id INTEGER PRIMARY KEY AUTOINCREMENT,
	lazy_flag BOOL NOT NULL,
	start_x BIGINT NOT NULL,
	end_x BIGINT NOT NULL,
	start_y BIGINT NOT NULL,
	end_y BIGINT NOT NULL,
	data BLOB,
	mask BLOB
	);